=begin
#Indexea OpenAPI

#这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ``` 

OpenAPI spec version: 1.0.0
Contact: indexea.com@gmail.com
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.54
=end

require 'spec_helper'
require 'json'

# Unit tests for IndexeaClient::RecommendApi
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'RecommendApi' do
  before do
    # run before each test
    @instance = IndexeaClient::RecommendApi.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of RecommendApi' do
    it 'should create an instance of RecommendApi' do
      expect(@instance).to be_instance_of(IndexeaClient::RecommendApi)
    end
  end

  # unit tests for recommend_click
  # 推荐结果点击行为收集
  # 该接口主要用于记录用户对推荐结果的点击行为
  # @param ident 推荐的唯一标识
  # @param action_id 对应推荐行为编号
  # @param doc_id 对应索引中的内部记录编号
  # @param [Hash] opts the optional parameters
  # @option opts [String] :userid 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
  # @option opts [String] :x_token 如果要使用非发布的组件，需要组件作者授权
  # @return [BOOLEAN]
  describe 'recommend_click test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for recommend_create
  # 创建新的推荐
  # @param body 推荐信息
  # @param app 应用标识
  # @param [Hash] opts the optional parameters
  # @return [RecommendBean]
  describe 'recommend_create test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for recommend_delete
  # 删除推荐
  # @param app 应用标识
  # @param id 推荐编号
  # @param [Hash] opts the optional parameters
  # @return [BOOLEAN]
  describe 'recommend_delete test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for recommend_detail
  # 获取推荐的记录列表
  # @param ident 推荐定义的标识
  # @param [Hash] opts the optional parameters
  # @option opts [String] :x_token 如果要使用非发布的组件，需要组件作者授权
  # @return [RecommendBean]
  describe 'recommend_detail test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for recommend_fetch
  # 获取推荐的记录列表
  # @param ident 推荐定义的标识
  # @param [Hash] opts the optional parameters
  # @option opts [String] :x_token 如果要使用非发布的组件，需要组件作者授权
  # @option opts [String] :userid 访客的唯一标识，该标识由搜索前端生成，长度不超过64
  # @option opts [Hash<String, String>] :condition 获取某个记录的参数,例如 id&#x3D;11223(后端将使用 term query 进行匹配)
  # @option opts [Integer] :from 起始值
  # @option opts [Integer] :count 推荐的记录数
  # @return [Object]
  describe 'recommend_fetch test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for recommend_list
  # 获取已定义的推荐列表
  # @param app 应用标识
  # @param [Hash] opts the optional parameters
  # @return [Array<RecommendBean>]
  describe 'recommend_list test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for recommend_update
  # 更新推荐信息
  # @param body 推荐信息
  # @param app 应用标识
  # @param [Hash] opts the optional parameters
  # @return [BOOLEAN]
  describe 'recommend_update test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
