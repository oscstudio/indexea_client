=begin
#Indexea OpenAPI

#这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ``` 

OpenAPI spec version: 1.0.0
Contact: indexea.com@gmail.com
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.54
=end

module IndexeaClient
  class WidgetsApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # 复制指定组件并创建新组件
    # @param app 应用标识
    # @param widget 源组件编号
    # @param [Hash] opts the optional parameters
    # @return [WidgetBean]
    def widget_copy(app, widget, opts = {})
      data, _status_code, _headers = widget_copy_with_http_info(app, widget, opts)
      data
    end

    # 复制指定组件并创建新组件
    # @param app 应用标识
    # @param widget 源组件编号
    # @param [Hash] opts the optional parameters
    # @return [Array<(WidgetBean, Integer, Hash)>] WidgetBean data, response status code and response headers
    def widget_copy_with_http_info(app, widget, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_copy ...'
      end
      # verify the required parameter 'app' is set
      if @api_client.config.client_side_validation && app.nil?
        fail ArgumentError, "Missing the required parameter 'app' when calling WidgetsApi.widget_copy"
      end
      # verify the required parameter 'widget' is set
      if @api_client.config.client_side_validation && widget.nil?
        fail ArgumentError, "Missing the required parameter 'widget' when calling WidgetsApi.widget_copy"
      end
      # resource path
      local_var_path = '/widget/{app}/copy'.sub('{' + 'app' + '}', app.to_s)

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'widget'] = widget

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'WidgetBean' 

      auth_names = opts[:auth_names] || ['TokenAuth']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_copy\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # 复制组件到已有组件
    # @param app 应用标识
    # @param widget 源组件编号
    # @param to 目标组件编号
    # @param [Hash] opts the optional parameters
    # @return [BOOLEAN]
    def widget_copy_to_widget(app, widget, to, opts = {})
      data, _status_code, _headers = widget_copy_to_widget_with_http_info(app, widget, to, opts)
      data
    end

    # 复制组件到已有组件
    # @param app 应用标识
    # @param widget 源组件编号
    # @param to 目标组件编号
    # @param [Hash] opts the optional parameters
    # @return [Array<(BOOLEAN, Integer, Hash)>] BOOLEAN data, response status code and response headers
    def widget_copy_to_widget_with_http_info(app, widget, to, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_copy_to_widget ...'
      end
      # verify the required parameter 'app' is set
      if @api_client.config.client_side_validation && app.nil?
        fail ArgumentError, "Missing the required parameter 'app' when calling WidgetsApi.widget_copy_to_widget"
      end
      # verify the required parameter 'widget' is set
      if @api_client.config.client_side_validation && widget.nil?
        fail ArgumentError, "Missing the required parameter 'widget' when calling WidgetsApi.widget_copy_to_widget"
      end
      # verify the required parameter 'to' is set
      if @api_client.config.client_side_validation && to.nil?
        fail ArgumentError, "Missing the required parameter 'to' when calling WidgetsApi.widget_copy_to_widget"
      end
      # resource path
      local_var_path = '/widget/{app}/copy'.sub('{' + 'app' + '}', app.to_s)

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'widget'] = widget
      query_params[:'to'] = to

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'BOOLEAN' 

      auth_names = opts[:auth_names] || ['TokenAuth']
      data, status_code, headers = @api_client.call_api(:PUT, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_copy_to_widget\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # 创建组件
    # @param body 
    # @param app 应用标识
    # @param [Hash] opts the optional parameters
    # @return [WidgetBean]
    def widget_create(body, app, opts = {})
      data, _status_code, _headers = widget_create_with_http_info(body, app, opts)
      data
    end

    # 创建组件
    # @param body 
    # @param app 应用标识
    # @param [Hash] opts the optional parameters
    # @return [Array<(WidgetBean, Integer, Hash)>] WidgetBean data, response status code and response headers
    def widget_create_with_http_info(body, app, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_create ...'
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling WidgetsApi.widget_create"
      end
      # verify the required parameter 'app' is set
      if @api_client.config.client_side_validation && app.nil?
        fail ArgumentError, "Missing the required parameter 'app' when calling WidgetsApi.widget_create"
      end
      # resource path
      local_var_path = '/widgets/{app}'.sub('{' + 'app' + '}', app.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] || @api_client.object_to_http_body(body) 

      return_type = opts[:return_type] || 'WidgetBean' 

      auth_names = opts[:auth_names] || ['TokenAuth']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_create\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # 删除组件
    # @param app 应用标识
    # @param widget 组件编号
    # @param [Hash] opts the optional parameters
    # @option opts [String] :password 
    # @return [BOOLEAN]
    def widget_delete(app, widget, opts = {})
      data, _status_code, _headers = widget_delete_with_http_info(app, widget, opts)
      data
    end

    # 删除组件
    # @param app 应用标识
    # @param widget 组件编号
    # @param [Hash] opts the optional parameters
    # @option opts [String] :password 
    # @return [Array<(BOOLEAN, Integer, Hash)>] BOOLEAN data, response status code and response headers
    def widget_delete_with_http_info(app, widget, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_delete ...'
      end
      # verify the required parameter 'app' is set
      if @api_client.config.client_side_validation && app.nil?
        fail ArgumentError, "Missing the required parameter 'app' when calling WidgetsApi.widget_delete"
      end
      # verify the required parameter 'widget' is set
      if @api_client.config.client_side_validation && widget.nil?
        fail ArgumentError, "Missing the required parameter 'widget' when calling WidgetsApi.widget_delete"
      end
      # resource path
      local_var_path = '/widgets/{app}/{widget}'.sub('{' + 'app' + '}', app.to_s).sub('{' + 'widget' + '}', widget.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/x-www-form-urlencoded'])

      # form parameters
      form_params = opts[:form_params] || {}
      form_params['password'] = opts[:'password'] if !opts[:'password'].nil?

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'BOOLEAN' 

      auth_names = opts[:auth_names] || ['TokenAuth']
      data, status_code, headers = @api_client.call_api(:DELETE, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_delete\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # 获取UI组件的所有相关信息
    # @param ident UI组件的唯一标识
    # @param [Hash] opts the optional parameters
    # @option opts [String] :x_token 如果要使用非发布的组件，需要组件作者授权
    # @return [WidgetBean]
    def widget_detail(ident, opts = {})
      data, _status_code, _headers = widget_detail_with_http_info(ident, opts)
      data
    end

    # 获取UI组件的所有相关信息
    # @param ident UI组件的唯一标识
    # @param [Hash] opts the optional parameters
    # @option opts [String] :x_token 如果要使用非发布的组件，需要组件作者授权
    # @return [Array<(WidgetBean, Integer, Hash)>] WidgetBean data, response status code and response headers
    def widget_detail_with_http_info(ident, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_detail ...'
      end
      # verify the required parameter 'ident' is set
      if @api_client.config.client_side_validation && ident.nil?
        fail ArgumentError, "Missing the required parameter 'ident' when calling WidgetsApi.widget_detail"
      end
      # resource path
      local_var_path = '/widget/{ident}'.sub('{' + 'ident' + '}', ident.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      header_params[:'x-token'] = opts[:'x_token'] if !opts[:'x_token'].nil?

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'WidgetBean' 

      auth_names = opts[:auth_names] || []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_detail\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # 下载组件应用源码
    # @param app 应用标识
    # @param widget 组件编号
    # @param framework 指定的技术框架
    # @param [Hash] opts the optional parameters
    # @return [String]
    def widget_download(app, widget, framework, opts = {})
      data, _status_code, _headers = widget_download_with_http_info(app, widget, framework, opts)
      data
    end

    # 下载组件应用源码
    # @param app 应用标识
    # @param widget 组件编号
    # @param framework 指定的技术框架
    # @param [Hash] opts the optional parameters
    # @return [Array<(String, Integer, Hash)>] String data, response status code and response headers
    def widget_download_with_http_info(app, widget, framework, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_download ...'
      end
      # verify the required parameter 'app' is set
      if @api_client.config.client_side_validation && app.nil?
        fail ArgumentError, "Missing the required parameter 'app' when calling WidgetsApi.widget_download"
      end
      # verify the required parameter 'widget' is set
      if @api_client.config.client_side_validation && widget.nil?
        fail ArgumentError, "Missing the required parameter 'widget' when calling WidgetsApi.widget_download"
      end
      # verify the required parameter 'framework' is set
      if @api_client.config.client_side_validation && framework.nil?
        fail ArgumentError, "Missing the required parameter 'framework' when calling WidgetsApi.widget_download"
      end
      # resource path
      local_var_path = '/widgets/{app}/{widget}/download'.sub('{' + 'app' + '}', app.to_s).sub('{' + 'widget' + '}', widget.to_s)

      # query parameters
      query_params = opts[:query_params] || {}
      query_params[:'framework'] = framework

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['default'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'String' 

      auth_names = opts[:auth_names] || ['TokenAuth']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_download\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # 获取组件的详情
    # @param app 应用标识
    # @param widget 组件编号
    # @param [Hash] opts the optional parameters
    # @return [WidgetBean]
    def widget_get(app, widget, opts = {})
      data, _status_code, _headers = widget_get_with_http_info(app, widget, opts)
      data
    end

    # 获取组件的详情
    # @param app 应用标识
    # @param widget 组件编号
    # @param [Hash] opts the optional parameters
    # @return [Array<(WidgetBean, Integer, Hash)>] WidgetBean data, response status code and response headers
    def widget_get_with_http_info(app, widget, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_get ...'
      end
      # verify the required parameter 'app' is set
      if @api_client.config.client_side_validation && app.nil?
        fail ArgumentError, "Missing the required parameter 'app' when calling WidgetsApi.widget_get"
      end
      # verify the required parameter 'widget' is set
      if @api_client.config.client_side_validation && widget.nil?
        fail ArgumentError, "Missing the required parameter 'widget' when calling WidgetsApi.widget_get"
      end
      # resource path
      local_var_path = '/widgets/{app}/{widget}'.sub('{' + 'app' + '}', app.to_s).sub('{' + 'widget' + '}', widget.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'WidgetBean' 

      auth_names = opts[:auth_names] || ['TokenAuth']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # 获取应用的组件列表
    # @param app 应用标识
    # @param [Hash] opts the optional parameters
    # @return [Array<WidgetBean>]
    def widget_list(app, opts = {})
      data, _status_code, _headers = widget_list_with_http_info(app, opts)
      data
    end

    # 获取应用的组件列表
    # @param app 应用标识
    # @param [Hash] opts the optional parameters
    # @return [Array<(Array<WidgetBean>, Integer, Hash)>] Array<WidgetBean> data, response status code and response headers
    def widget_list_with_http_info(app, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_list ...'
      end
      # verify the required parameter 'app' is set
      if @api_client.config.client_side_validation && app.nil?
        fail ArgumentError, "Missing the required parameter 'app' when calling WidgetsApi.widget_list"
      end
      # resource path
      local_var_path = '/widgets/{app}'.sub('{' + 'app' + '}', app.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'Array<WidgetBean>' 

      auth_names = opts[:auth_names] || ['TokenAuth']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_list\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # 设置组件 Logo
    # @param app 应用标识
    # @param widget 组件编号
    # @param [Hash] opts the optional parameters
    # @option opts [String] :logo 
    # @return [WidgetLogo]
    def widget_logo(app, widget, opts = {})
      data, _status_code, _headers = widget_logo_with_http_info(app, widget, opts)
      data
    end

    # 设置组件 Logo
    # @param app 应用标识
    # @param widget 组件编号
    # @param [Hash] opts the optional parameters
    # @option opts [String] :logo 
    # @return [Array<(WidgetLogo, Integer, Hash)>] WidgetLogo data, response status code and response headers
    def widget_logo_with_http_info(app, widget, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_logo ...'
      end
      # verify the required parameter 'app' is set
      if @api_client.config.client_side_validation && app.nil?
        fail ArgumentError, "Missing the required parameter 'app' when calling WidgetsApi.widget_logo"
      end
      # verify the required parameter 'widget' is set
      if @api_client.config.client_side_validation && widget.nil?
        fail ArgumentError, "Missing the required parameter 'widget' when calling WidgetsApi.widget_logo"
      end
      # resource path
      local_var_path = '/widgets/{app}/{widget}/logo'.sub('{' + 'app' + '}', app.to_s).sub('{' + 'widget' + '}', widget.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['multipart/form-data'])

      # form parameters
      form_params = opts[:form_params] || {}
      form_params['logo'] = opts[:'logo'] if !opts[:'logo'].nil?

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'WidgetLogo' 

      auth_names = opts[:auth_names] || ['TokenAuth']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_logo\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # 修改组件
    # @param body 
    # @param app 应用标识
    # @param widget 组件编号
    # @param [Hash] opts the optional parameters
    # @return [WidgetBean]
    def widget_update(body, app, widget, opts = {})
      data, _status_code, _headers = widget_update_with_http_info(body, app, widget, opts)
      data
    end

    # 修改组件
    # @param body 
    # @param app 应用标识
    # @param widget 组件编号
    # @param [Hash] opts the optional parameters
    # @return [Array<(WidgetBean, Integer, Hash)>] WidgetBean data, response status code and response headers
    def widget_update_with_http_info(body, app, widget, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_update ...'
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling WidgetsApi.widget_update"
      end
      # verify the required parameter 'app' is set
      if @api_client.config.client_side_validation && app.nil?
        fail ArgumentError, "Missing the required parameter 'app' when calling WidgetsApi.widget_update"
      end
      # verify the required parameter 'widget' is set
      if @api_client.config.client_side_validation && widget.nil?
        fail ArgumentError, "Missing the required parameter 'widget' when calling WidgetsApi.widget_update"
      end
      # resource path
      local_var_path = '/widgets/{app}/{widget}'.sub('{' + 'app' + '}', app.to_s).sub('{' + 'widget' + '}', widget.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json'])

      # form parameters
      form_params = opts[:form_params] || {}

      # http body (model)
      post_body = opts[:body] || @api_client.object_to_http_body(body) 

      return_type = opts[:return_type] || 'WidgetBean' 

      auth_names = opts[:auth_names] || ['TokenAuth']
      data, status_code, headers = @api_client.call_api(:PUT, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_update\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # 修改组件设置参数
    # @param key 
    # @param type 
    # @param value 
    # @param vcode 
    # @param app 应用标识
    # @param widget 组件编号
    # @param [Hash] opts the optional parameters
    # @return [BOOLEAN]
    def widget_update_settings(key, type, value, vcode, app, widget, opts = {})
      data, _status_code, _headers = widget_update_settings_with_http_info(key, type, value, vcode, app, widget, opts)
      data
    end

    # 修改组件设置参数
    # @param key 
    # @param type 
    # @param value 
    # @param vcode 
    # @param app 应用标识
    # @param widget 组件编号
    # @param [Hash] opts the optional parameters
    # @return [Array<(BOOLEAN, Integer, Hash)>] BOOLEAN data, response status code and response headers
    def widget_update_settings_with_http_info(key, type, value, vcode, app, widget, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: WidgetsApi.widget_update_settings ...'
      end
      # verify the required parameter 'key' is set
      if @api_client.config.client_side_validation && key.nil?
        fail ArgumentError, "Missing the required parameter 'key' when calling WidgetsApi.widget_update_settings"
      end
      # verify the required parameter 'type' is set
      if @api_client.config.client_side_validation && type.nil?
        fail ArgumentError, "Missing the required parameter 'type' when calling WidgetsApi.widget_update_settings"
      end
      # verify enum value
      if @api_client.config.client_side_validation && !['int', 'bool', 'string'].include?(type)
        fail ArgumentError, "invalid value for 'type', must be one of int, bool, string"
      end
      # verify the required parameter 'value' is set
      if @api_client.config.client_side_validation && value.nil?
        fail ArgumentError, "Missing the required parameter 'value' when calling WidgetsApi.widget_update_settings"
      end
      # verify the required parameter 'vcode' is set
      if @api_client.config.client_side_validation && vcode.nil?
        fail ArgumentError, "Missing the required parameter 'vcode' when calling WidgetsApi.widget_update_settings"
      end
      # verify the required parameter 'app' is set
      if @api_client.config.client_side_validation && app.nil?
        fail ArgumentError, "Missing the required parameter 'app' when calling WidgetsApi.widget_update_settings"
      end
      # verify the required parameter 'widget' is set
      if @api_client.config.client_side_validation && widget.nil?
        fail ArgumentError, "Missing the required parameter 'widget' when calling WidgetsApi.widget_update_settings"
      end
      # resource path
      local_var_path = '/widgets/{app}/{widget}'.sub('{' + 'app' + '}', app.to_s).sub('{' + 'widget' + '}', widget.to_s)

      # query parameters
      query_params = opts[:query_params] || {}

      # header parameters
      header_params = opts[:header_params] || {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/x-www-form-urlencoded'])

      # form parameters
      form_params = opts[:form_params] || {}
      form_params['key'] = key
      form_params['type'] = type
      form_params['value'] = value
      form_params['vcode'] = vcode

      # http body (model)
      post_body = opts[:body] 

      return_type = opts[:return_type] || 'BOOLEAN' 

      auth_names = opts[:auth_names] || ['TokenAuth']
      data, status_code, headers = @api_client.call_api(:PATCH, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => return_type)

      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: WidgetsApi#widget_update_settings\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
