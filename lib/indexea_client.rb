=begin
#Indexea OpenAPI

#这是 Indexea 搜索服务平台的 OpenAPI，用于描述平台的所有接口信息，你可以通过这个页面来了解和在线验证平台的所有接口信息。  ### Errors  本 API 使用标准的 HTTP 状态码来指示操作成功或者失败，如果失败将会在 body 中以 JSON 格式提供详细的错误信息，如下所示：  ``` {   \"error\": 404,   \"message\": \"page not found\" } ``` 

OpenAPI spec version: 1.0.0
Contact: indexea.com@gmail.com
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.54
=end

# Common files
require 'indexea_client/api_client'
require 'indexea_client/api_error'
require 'indexea_client/version'
require 'indexea_client/configuration'

# Models
require 'indexea_client/models/account_bean'
require 'indexea_client/models/account_passwd_body'
require 'indexea_client/models/account_portrait_body'
require 'indexea_client/models/account_profile_body'
require 'indexea_client/models/account_resetpwd_body'
require 'indexea_client/models/account_signin_body'
require 'indexea_client/models/account_signup_body'
require 'indexea_client/models/accounts_feedback_body'
require 'indexea_client/models/accounts_message_body'
require 'indexea_client/models/analyze_object'
require 'indexea_client/models/analyze_token'
require 'indexea_client/models/app_bean'
require 'indexea_client/models/app_company_body'
require 'indexea_client/models/app_index_body'
require 'indexea_client/models/app_log_account'
require 'indexea_client/models/app_log_bean'
require 'indexea_client/models/app_logs_bean'
require 'indexea_client/models/app_mentor_bean'
require 'indexea_client/models/app_oauth_body'
require 'indexea_client/models/app_oauth_body_1'
require 'indexea_client/models/app_oauthresetsecret_body'
require 'indexea_client/models/app_tokens_body'
require 'indexea_client/models/app_tokens_body_1'
require 'indexea_client/models/app_widget_body'
require 'indexea_client/models/apps_app_body'
require 'indexea_client/models/apps_body'
require 'indexea_client/models/auto_complete_item'
require 'indexea_client/models/blacklist_bean'
require 'indexea_client/models/bulletin'
require 'indexea_client/models/company_bean'
require 'indexea_client/models/contact_form'
require 'indexea_client/models/crawler_log'
require 'indexea_client/models/crawler_logs'
require 'indexea_client/models/crawler_task'
require 'indexea_client/models/global_option_form'
require 'indexea_client/models/index_bean'
require 'indexea_client/models/index_bulk_body'
require 'indexea_client/models/index_field_bean'
require 'indexea_client/models/index_form'
require 'indexea_client/models/index_rebuild_form'
require 'indexea_client/models/index_settings'
require 'indexea_client/models/index_settings_body'
require 'indexea_client/models/index_stat_bean'
require 'indexea_client/models/index_synonymsflush_body'
require 'indexea_client/models/index_task'
require 'indexea_client/models/index_template'
require 'indexea_client/models/index_templates'
require 'indexea_client/models/index_upload_body'
require 'indexea_client/models/intelligent_mapping'
require 'indexea_client/models/keyword_binding_bean'
require 'indexea_client/models/mentor_form'
require 'indexea_client/models/message'
require 'indexea_client/models/messages'
require 'indexea_client/models/oauth_app_bean'
require 'indexea_client/models/openid_bean'
require 'indexea_client/models/option_form'
require 'indexea_client/models/orders_ident_body'
require 'indexea_client/models/pay_result'
require 'indexea_client/models/payment_invoice'
require 'indexea_client/models/payment_order'
require 'indexea_client/models/payment_record'
require 'indexea_client/models/payment_service'
require 'indexea_client/models/query_action_bean'
require 'indexea_client/models/query_bean'
require 'indexea_client/models/query_form'
require 'indexea_client/models/query_node'
require 'indexea_client/models/query_sort_field'
require 'indexea_client/models/query_variable_bean'
require 'indexea_client/models/recommend_bean'
require 'indexea_client/models/record_filter'
require 'indexea_client/models/search_estimate_result'
require 'indexea_client/models/search_word'
require 'indexea_client/models/stat_index_bean'
require 'indexea_client/models/synonyms_bean'
require 'indexea_client/models/token_bean'
require 'indexea_client/models/trigger_bean'
require 'indexea_client/models/trigger_log_bean'
require 'indexea_client/models/value_of_field'
require 'indexea_client/models/widget_bean'
require 'indexea_client/models/widget_form'
require 'indexea_client/models/widget_logo'
require 'indexea_client/models/widget_logo_body'
require 'indexea_client/models/widget_status_form'

# APIs
require 'indexea_client/api/account_api'
require 'indexea_client/api/apps_api'
require 'indexea_client/api/fields_api'
require 'indexea_client/api/global_api'
require 'indexea_client/api/indices_api'
require 'indexea_client/api/message_api'
require 'indexea_client/api/payment_api'
require 'indexea_client/api/queries_api'
require 'indexea_client/api/recommend_api'
require 'indexea_client/api/records_api'
require 'indexea_client/api/search_api'
require 'indexea_client/api/stats_api'
require 'indexea_client/api/widgets_api'

module IndexeaClient
  class << self
    # Customize default settings for the SDK using block.
    #   IndexeaClient.configure do |config|
    #     config.username = "xxx"
    #     config.password = "xxx"
    #   end
    # If no block given, return the default Configuration object.
    def configure
      if block_given?
        yield(Configuration.default)
      else
        Configuration.default
      end
    end
  end
end
