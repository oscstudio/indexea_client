# IndexeaClient::PaymentRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**ident** | **String** |  | [optional] 
**method** | **String** |  | [optional] 
**amount** | **Integer** |  | [optional] 
**params** | **Object** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**status** | **Integer** |  | [optional] 

