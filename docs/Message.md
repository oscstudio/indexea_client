# IndexeaClient::Message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**account** | **Integer** |  | [optional] 
**sender** | **Integer** |  | [optional] 
**receiver** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**avater** | **String** |  | [optional] 
**msg** | **String** |  | [optional] 
**type** | **Integer** |  | [optional] 
**msgid** | **Integer** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**read_at** | **DateTime** |  | [optional] 
**status** | **Integer** |  | [optional] 

