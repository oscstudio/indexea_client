# IndexeaClient::IndexForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**intro** | **String** |  | [optional] 
**_alias** | **Array&lt;String&gt;** |  | [optional] 
**type** | **String** |  | [optional] 
**shards** | **Integer** | 该索引的分片数 | [optional] 
**replicas** | **Integer** | 该索引的副本数 | [optional] 
**analyzer** | **String** |  | [optional] 
**search_analyzer** | **String** |  | [optional] 
**status** | **Integer** |  | [optional] 

