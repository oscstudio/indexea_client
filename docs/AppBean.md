# IndexeaClient::AppBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**account** | **Integer** | 创建者 | [optional] 
**cluster** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**ident** | **String** | 应用唯一标识 | [optional] 
**intro** | **String** |  | [optional] 
**paid** | **BOOLEAN** | 是否付费应用 | [optional] 
**storage** | **Integer** | 存储空间 | [optional] 
**shards** | **Integer** | 分片数 | [optional] 
**replicas** | **Integer** | 副本数 | [optional] 
**searchs** | **Integer** | 搜索次数 | [optional] 
**mentor** | [**AppMentorBean**](AppMentorBean.md) |  | [optional] 
**settings** | **Object** | 应用设置 | [optional] 
**indices** | **Integer** | 应用索引数 | [optional] 
**created_at** | **DateTime** |  | [optional] 
**expired_at** | **DateTime** |  | [optional] 
**status** | **Integer** |  | [optional] 

