# IndexeaClient::SynonymsBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**index** | **Integer** |  | [optional] 
**type** | **Integer** |  | [optional] 
**words** | **Array&lt;String&gt;** |  | [optional] 
**synonyms** | **Array&lt;String&gt;** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**status** | **Integer** |  | [optional] 

