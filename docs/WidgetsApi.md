# IndexeaClient::WidgetsApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**widget_copy**](WidgetsApi.md#widget_copy) | **POST** /widget/{app}/copy | 复制指定组件并创建新组件
[**widget_copy_to_widget**](WidgetsApi.md#widget_copy_to_widget) | **PUT** /widget/{app}/copy | 复制组件到已有组件
[**widget_create**](WidgetsApi.md#widget_create) | **POST** /widgets/{app} | 创建组件
[**widget_delete**](WidgetsApi.md#widget_delete) | **DELETE** /widgets/{app}/{widget} | 删除组件
[**widget_detail**](WidgetsApi.md#widget_detail) | **GET** /widget/{ident} | 获取UI组件的所有相关信息
[**widget_download**](WidgetsApi.md#widget_download) | **GET** /widgets/{app}/{widget}/download | 下载组件应用源码
[**widget_get**](WidgetsApi.md#widget_get) | **GET** /widgets/{app}/{widget} | 获取组件的详情
[**widget_list**](WidgetsApi.md#widget_list) | **GET** /widgets/{app} | 获取应用的组件列表
[**widget_logo**](WidgetsApi.md#widget_logo) | **POST** /widgets/{app}/{widget}/logo | 设置组件 Logo
[**widget_update**](WidgetsApi.md#widget_update) | **PUT** /widgets/{app}/{widget} | 修改组件
[**widget_update_settings**](WidgetsApi.md#widget_update_settings) | **PATCH** /widgets/{app}/{widget} | 修改组件设置参数

# **widget_copy**
> WidgetBean widget_copy(app, widget)

复制指定组件并创建新组件

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::WidgetsApi.new
app = 'app_example' # String | 应用标识
widget = 56 # Integer | 源组件编号


begin
  #复制指定组件并创建新组件
  result = api_instance.widget_copy(app, widget)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_copy: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **widget** | **Integer**| 源组件编号 | 

### Return type

[**WidgetBean**](WidgetBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **widget_copy_to_widget**
> BOOLEAN widget_copy_to_widget(app, widget, to)

复制组件到已有组件

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::WidgetsApi.new
app = 'app_example' # String | 应用标识
widget = 56 # Integer | 源组件编号
to = 56 # Integer | 目标组件编号


begin
  #复制组件到已有组件
  result = api_instance.widget_copy_to_widget(app, widget, to)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_copy_to_widget: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **widget** | **Integer**| 源组件编号 | 
 **to** | **Integer**| 目标组件编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **widget_create**
> WidgetBean widget_create(bodyapp)

创建组件

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::WidgetsApi.new
body = IndexeaClient::WidgetForm.new # WidgetForm | 
app = 'app_example' # String | 应用标识


begin
  #创建组件
  result = api_instance.widget_create(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WidgetForm**](WidgetForm.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

[**WidgetBean**](WidgetBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **widget_delete**
> BOOLEAN widget_delete(appwidget, opts)

删除组件

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::WidgetsApi.new
app = 'app_example' # String | 应用标识
widget = 56 # Integer | 组件编号
opts = { 
  password: 'password_example' # String | 
}

begin
  #删除组件
  result = api_instance.widget_delete(appwidget, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **widget** | **Integer**| 组件编号 | 
 **password** | [**String**](.md)|  | [optional] 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **widget_detail**
> WidgetBean widget_detail(ident, opts)

获取UI组件的所有相关信息

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::WidgetsApi.new
ident = 'ident_example' # String | UI组件的唯一标识
opts = { 
  x_token: 'x_token_example' # String | 如果要使用非发布的组件，需要组件作者授权
}

begin
  #获取UI组件的所有相关信息
  result = api_instance.widget_detail(ident, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_detail: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **String**| UI组件的唯一标识 | 
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 

### Return type

[**WidgetBean**](WidgetBean.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **widget_download**
> String widget_download(app, widget, framework)

下载组件应用源码

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::WidgetsApi.new
app = 'app_example' # String | 应用标识
widget = 56 # Integer | 组件编号
framework = 'framework_example' # String | 指定的技术框架


begin
  #下载组件应用源码
  result = api_instance.widget_download(app, widget, framework)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_download: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **widget** | **Integer**| 组件编号 | 
 **framework** | **String**| 指定的技术框架 | 

### Return type

**String**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: default



# **widget_get**
> WidgetBean widget_get(app, widget)

获取组件的详情

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::WidgetsApi.new
app = 'app_example' # String | 应用标识
widget = 56 # Integer | 组件编号


begin
  #获取组件的详情
  result = api_instance.widget_get(app, widget)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **widget** | **Integer**| 组件编号 | 

### Return type

[**WidgetBean**](WidgetBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **widget_list**
> Array&lt;WidgetBean&gt; widget_list(app)

获取应用的组件列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::WidgetsApi.new
app = 'app_example' # String | 应用标识


begin
  #获取应用的组件列表
  result = api_instance.widget_list(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**Array&lt;WidgetBean&gt;**](WidgetBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **widget_logo**
> WidgetLogo widget_logo(appwidget, opts)

设置组件 Logo

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::WidgetsApi.new
app = 'app_example' # String | 应用标识
widget = 56 # Integer | 组件编号
opts = { 
  logo: 'logo_example' # String | 
}

begin
  #设置组件 Logo
  result = api_instance.widget_logo(appwidget, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_logo: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **widget** | **Integer**| 组件编号 | 
 **logo** | **String**|  | [optional] 

### Return type

[**WidgetLogo**](WidgetLogo.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json



# **widget_update**
> WidgetBean widget_update(bodyappwidget)

修改组件

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::WidgetsApi.new
body = IndexeaClient::WidgetForm.new # WidgetForm | 
app = 'app_example' # String | 应用标识
widget = 56 # Integer | 组件编号


begin
  #修改组件
  result = api_instance.widget_update(bodyappwidget)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WidgetForm**](WidgetForm.md)|  | 
 **app** | **String**| 应用标识 | 
 **widget** | **Integer**| 组件编号 | 

### Return type

[**WidgetBean**](WidgetBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **widget_update_settings**
> BOOLEAN widget_update_settings(keytypevaluevcodeappwidget)

修改组件设置参数

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::WidgetsApi.new
key = 'key_example' # String | 
type = 'type_example' # String | 
value = 'value_example' # String | 
vcode = 'vcode_example' # String | 
app = 'app_example' # String | 应用标识
widget = 56 # Integer | 组件编号


begin
  #修改组件设置参数
  result = api_instance.widget_update_settings(keytypevaluevcodeappwidget)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling WidgetsApi->widget_update_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**|  | 
 **type** | **String**|  | 
 **value** | **String**|  | 
 **vcode** | **String**|  | 
 **app** | **String**| 应用标识 | 
 **widget** | **Integer**| 组件编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



