# IndexeaClient::WidgetBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**ident** | **String** |  | [optional] 
**account** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**type** | **String** |  | [optional] [default to &#x27;web&#x27;]
**sub_type** | **String** |  | [optional] [default to &#x27;None&#x27;]
**intro** | **String** |  | [optional] 
**queries** | [**Array&lt;QueryBean&gt;**](QueryBean.md) |  | [optional] 
**layout** | **String** | 组件布局 | [optional] 
**settings** | **Object** | 组件设置参数 | [optional] 
**download_url** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**status** | **Integer** |  | [optional] 

