# IndexeaClient::AccountApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**account_bulletin**](AccountApi.md#account_bulletin) | **GET** /accounts/bulletin | 获取系统公告
[**account_delete**](AccountApi.md#account_delete) | **DELETE** /account/profile | 注销账号
[**account_delete_openid**](AccountApi.md#account_delete_openid) | **DELETE** /account/openid | 解绑三方账号
[**account_openid**](AccountApi.md#account_openid) | **GET** /account/openid | 获取绑定的所有三方账号
[**account_passwd**](AccountApi.md#account_passwd) | **POST** /account/passwd | 修改账号密码
[**account_portrait**](AccountApi.md#account_portrait) | **POST** /account/portrait | 修改账号头像
[**account_profile**](AccountApi.md#account_profile) | **GET** /account/profile | 获取登录账号信息
[**account_reset_pwd**](AccountApi.md#account_reset_pwd) | **POST** /account/reset-pwd | 重置账号密码
[**account_send_verify_code**](AccountApi.md#account_send_verify_code) | **GET** /account/send-verify-code | 发送账号验证码
[**account_signin**](AccountApi.md#account_signin) | **POST** /account/signin | 登录系统
[**account_signout**](AccountApi.md#account_signout) | **POST** /account/sign-out | 退出登录状态
[**account_signup**](AccountApi.md#account_signup) | **POST** /account/signup | 注册新账号
[**account_update**](AccountApi.md#account_update) | **POST** /account/profile | 修改账号资料
[**account_update_settings**](AccountApi.md#account_update_settings) | **POST** /account/settings | 修改账号设置

# **account_bulletin**
> Bulletin account_bulletin

获取系统公告

获取系统公告

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AccountApi.new

begin
  #获取系统公告
  result = api_instance.account_bulletin
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_bulletin: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Bulletin**](Bulletin.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **account_delete**
> BOOLEAN account_delete(opts)

注销账号

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AccountApi.new
opts = { 
  pwd: 'pwd_example' # String | 
  vcode: 'vcode_example' # String | 
}

begin
  #注销账号
  result = api_instance.account_delete(opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pwd** | [**String**](.md)|  | [optional] 
 **vcode** | **String**|  | [optional] 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **account_delete_openid**
> BOOLEAN account_delete_openid(type, openid)

解绑三方账号

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AccountApi.new
type = 'type_example' # String | 三方账号类型
openid = 'openid_example' # String | 三方账号唯一标识


begin
  #解绑三方账号
  result = api_instance.account_delete_openid(type, openid)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_delete_openid: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**| 三方账号类型 | 
 **openid** | **String**| 三方账号唯一标识 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **account_openid**
> Array&lt;OpenidBean&gt; account_openid

获取绑定的所有三方账号

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AccountApi.new

begin
  #获取绑定的所有三方账号
  result = api_instance.account_openid
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_openid: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Array&lt;OpenidBean&gt;**](OpenidBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **account_passwd**
> BOOLEAN account_passwd(passwordnew_password)

修改账号密码

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AccountApi.new
password = 'password_example' # String | 
new_password = 'new_password_example' # String | 


begin
  #修改账号密码
  result = api_instance.account_passwd(passwordnew_password)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_passwd: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password** | [**String**](.md)|  | 
 **new_password** | [**String**](.md)|  | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **account_portrait**
> AccountBean account_portrait(portrait)

修改账号头像

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AccountApi.new
portrait = 'portrait_example' # String | 


begin
  #修改账号头像
  result = api_instance.account_portrait(portrait)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_portrait: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **portrait** | **String**|  | 

### Return type

[**AccountBean**](AccountBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json



# **account_profile**
> AccountBean account_profile(opts)

获取登录账号信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AccountApi.new
opts = { 
  account: 'account_example' # String | 获取指定账号信息，如果不指定，则返回当前登录账号
}

begin
  #获取登录账号信息
  result = api_instance.account_profile(opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_profile: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| 获取指定账号信息，如果不指定，则返回当前登录账号 | [optional] 

### Return type

[**AccountBean**](AccountBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **account_reset_pwd**
> BOOLEAN account_reset_pwd(opts)

重置账号密码

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::AccountApi.new
opts = { 
  account: 'account_example' # String | 
  verify_code: 'verify_code_example' # String | 
  pwd: 'pwd_example' # String | 
}

begin
  #重置账号密码
  result = api_instance.account_reset_pwd(opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_reset_pwd: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**|  | [optional] 
 **verify_code** | **String**|  | [optional] 
 **pwd** | [**String**](.md)|  | [optional] 

### Return type

**BOOLEAN**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **account_send_verify_code**
> BOOLEAN account_send_verify_code(account, purpose)

发送账号验证码

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::AccountApi.new
account = 'account_example' # String | 账号
purpose = 'purpose_example' # String | 验证码的用途


begin
  #发送账号验证码
  result = api_instance.account_send_verify_code(account, purpose)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_send_verify_code: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**| 账号 | 
 **purpose** | **String**| 验证码的用途 | 

### Return type

**BOOLEAN**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **account_signin**
> AccountBean account_signin(accountpwdkeep_login)

登录系统

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::AccountApi.new
account = 'account_example' # String | 
pwd = 'pwd_example' # String | 
keep_login = true # BOOLEAN | 


begin
  #登录系统
  result = api_instance.account_signin(accountpwdkeep_login)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_signin: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**|  | 
 **pwd** | [**String**](.md)|  | 
 **keep_login** | **BOOLEAN**|  | 

### Return type

[**AccountBean**](AccountBean.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **account_signout**
> account_signout

退出登录状态

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AccountApi.new

begin
  #退出登录状态
  api_instance.account_signout
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_signout: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **account_signup**
> AccountBean account_signup(opts)

注册新账号

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::AccountApi.new
opts = { 
  account: 'account_example' # String | 
  pwd: 'pwd_example' # String | 
  name: 'name_example' # String | 
  activate_code: 'activate_code_example' # String | 
}

begin
  #注册新账号
  result = api_instance.account_signup(opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_signup: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **String**|  | [optional] 
 **pwd** | [**String**](.md)|  | [optional] 
 **name** | **String**|  | [optional] 
 **activate_code** | **String**|  | [optional] 

### Return type

[**AccountBean**](AccountBean.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **account_update**
> AccountBean account_update(body)

修改账号资料

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AccountApi.new
body = IndexeaClient::AccountBean.new # AccountBean | 


begin
  #修改账号资料
  result = api_instance.account_update(body)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AccountBean**](AccountBean.md)|  | 

### Return type

[**AccountBean**](AccountBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **account_update_settings**
> BOOLEAN account_update_settings(keytypevaluevcode)

修改账号设置

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AccountApi.new
key = 'key_example' # String | 
type = 'type_example' # String | 
value = 'value_example' # String | 
vcode = 'vcode_example' # String | 


begin
  #修改账号设置
  result = api_instance.account_update_settings(keytypevaluevcode)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AccountApi->account_update_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**|  | 
 **type** | **String**|  | 
 **value** | **String**|  | 
 **vcode** | **String**|  | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



