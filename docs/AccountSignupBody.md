# IndexeaClient::AccountSignupBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **String** |  | 
**pwd** | **String** |  | 
**name** | **String** |  | 
**activate_code** | **String** |  | 

