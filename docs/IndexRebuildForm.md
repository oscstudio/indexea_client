# IndexeaClient::IndexRebuildForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shards** | **Integer** | 该索引的分片数 | [optional] 
**replicas** | **Integer** | 该索引的副本数 | [optional] 
**analyzer** | **String** |  | [optional] 
**search_analyzer** | **String** |  | [optional] 
**fields** | **Object** |  | [optional] 
**vcode** | **String** |  | [optional] 

