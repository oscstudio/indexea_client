# IndexeaClient::AppMentorBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | **Integer** | 应用编号 | [optional] 
**account** | **Integer** | 成员编号 | [optional] 
**name** | **String** | 成员备注名 | [optional] 
**app_name** | **String** | 成员对应用的备注名 | [optional] 
**scopes** | **String** | 成员权限 | [optional] 
**report** | **Integer** | 是否接收报告 | [optional] 
**settings** | **Object** | 成员的设置项 | [optional] 
**created_at** | **DateTime** |  | [optional] 

