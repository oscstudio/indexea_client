# IndexeaClient::IndexSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **Integer** |  | [optional] 
**type** | **String** |  | [optional] 
**settings** | **Object** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 

