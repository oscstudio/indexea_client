# IndexeaClient::CrawlerLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**index** | **Integer** |  | [optional] 
**home_url** | **String** |  | [optional] 
**begin_at** | **DateTime** |  | [optional] 
**end_at** | **DateTime** |  | [optional] 
**used_seconds** | **Integer** |  | [optional] 
**success_count** | **Integer** |  | [optional] 
**failed_count** | **Integer** |  | [optional] 

