# IndexeaClient::QueryNode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **Integer** |  | [optional] 
**query** | **Integer** |  | [optional] 
**nodes** | **Object** |  | [optional] 
**saved_at** | **DateTime** |  | [optional] 

