# IndexeaClient::AccountResetpwdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **String** |  | 
**verify_code** | **String** |  | 
**pwd** | **String** |  | 

