# IndexeaClient::StatIndexBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | **Integer** |  | [optional] 
**index** | **Integer** |  | [optional] 
**date** | **Date** |  | [optional] 
**storage** | **Integer** |  | [optional] 
**searchs** | **Integer** |  | [optional] 
**errors** | **Integer** |  | [optional] 
**records** | **Integer** |  | [optional] 
**avg_time** | **String** |  | [optional] 

