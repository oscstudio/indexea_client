# IndexeaClient::QueryForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**intro** | **String** |  | [optional] 
**indices** | **Array&lt;Integer&gt;** | 关联的索引列表 | [optional] 
**query** | **Object** | 查询条件 | [optional] 
**suggest** | **Object** |  | [optional] 
**script_score** | **Object** |  | [optional] 
**script_fields** | **Object** |  | [optional] 
**sort_fields** | [**Array&lt;QuerySortField&gt;**](QuerySortField.md) |  | [optional] 
**status** | **Integer** |  | [optional] 

