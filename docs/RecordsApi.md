# IndexeaClient::RecordsApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**record_delete**](RecordsApi.md#record_delete) | **DELETE** /records/{app}/{index} | 删除记录数据
[**record_delete_by_query**](RecordsApi.md#record_delete_by_query) | **DELETE** /records/{app}/{index}/bulk | 批量删除记录数据(delete_by_query)，该接口传递查询参数获取要删除的记录并逐一删除
[**record_get**](RecordsApi.md#record_get) | **GET** /records/{app}/{index} | 获取单条记录详情
[**record_list**](RecordsApi.md#record_list) | **POST** /records/{app}/{index} | 获取索引记录列表
[**record_push**](RecordsApi.md#record_push) | **PUT** /records/{app}/{index} | 插入或者更新索引数据
[**record_update_by_query**](RecordsApi.md#record_update_by_query) | **PATCH** /records/{app}/{index}/bulk | 批量修改记录数据(update_by_query)，该接口传递查询参数获取要更新的记录，并使用 body 中的对象进行记录合并更新
[**record_upload**](RecordsApi.md#record_upload) | **POST** /records/{app}/{index}/bulk | 上传记录
[**record_upload_0**](RecordsApi.md#record_upload_0) | **POST** /records/{app}/{index}/upload | 上传记录，该接口已经废弃，请使用 /records/{app}/{index}/batch 接口

# **record_delete**
> BOOLEAN record_delete(app, index, _id)

删除记录数据

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecordsApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
_id = ['_id_example'] # Array<String> | 主键字段值


begin
  #删除记录数据
  result = api_instance.record_delete(app, index, _id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecordsApi->record_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **_id** | [**Array&lt;String&gt;**](String.md)| 主键字段值 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **record_delete_by_query**
> Object record_delete_by_query(app, index, query, opts)

批量删除记录数据(delete_by_query)，该接口传递查询参数获取要删除的记录并逐一删除

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecordsApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
query = 56 # Integer | 查询编号
opts = { 
  params: {'key' => 'params_example'} # Hash<String, String> | 查询参数
}

begin
  #批量删除记录数据(delete_by_query)，该接口传递查询参数获取要删除的记录并逐一删除
  result = api_instance.record_delete_by_query(app, index, query, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecordsApi->record_delete_by_query: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **query** | **Integer**| 查询编号 | 
 **params** | [**Hash&lt;String, String&gt;**](String.md)| 查询参数 | [optional] 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **record_get**
> Object record_get(app, index, _id)

获取单条记录详情

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecordsApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
_id = '_id_example' # String | 记录 _id 值


begin
  #获取单条记录详情
  result = api_instance.record_get(app, index, _id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecordsApi->record_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **_id** | **String**| 记录 _id 值 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **record_list**
> Array&lt;Object&gt; record_list(appindex, opts)

获取索引记录列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecordsApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
opts = { 
  body: IndexeaClient::RecordFilter.new # RecordFilter | 
  q: 'q_example' # String | 搜索关键字
  field: 'field_example' # String | 搜索字段
  from: 0 # Integer | 起始记录
  size: 20 # Integer | 获取记录数
  save_filter: false # BOOLEAN | 是否保存过滤器信息
}

begin
  #获取索引记录列表
  result = api_instance.record_list(appindex, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecordsApi->record_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **body** | [**RecordFilter**](RecordFilter.md)|  | [optional] 
 **q** | **String**| 搜索关键字 | [optional] 
 **field** | **String**| 搜索字段 | [optional] 
 **from** | **Integer**| 起始记录 | [optional] [default to 0]
 **size** | **Integer**| 获取记录数 | [optional] [default to 20]
 **save_filter** | **BOOLEAN**| 是否保存过滤器信息 | [optional] [default to false]

### Return type

**Array&lt;Object&gt;**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **record_push**
> BOOLEAN record_push(bodyappindex, opts)

插入或者更新索引数据

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecordsApi.new
body = nil # Array<Object> | 
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
opts = { 
  combine: false # BOOLEAN | 更新策略：合并还是替换，combine=true 为合并模式
}

begin
  #插入或者更新索引数据
  result = api_instance.record_push(bodyappindex, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecordsApi->record_push: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Array&lt;Object&gt;**](Object.md)|  | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **combine** | **BOOLEAN**| 更新策略：合并还是替换，combine&#x3D;true 为合并模式 | [optional] [default to false]

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **record_update_by_query**
> Object record_update_by_query(bodyqueryappindex, opts)

批量修改记录数据(update_by_query)，该接口传递查询参数获取要更新的记录，并使用 body 中的对象进行记录合并更新

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecordsApi.new
body = nil # Object | 要更新的字段和对应的新字段值，只支持最基本类型
query = 56 # Integer | 查询编号
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
opts = { 
  params: {'key' => 'params_example'} # Hash<String, String> | 查询参数
}

begin
  #批量修改记录数据(update_by_query)，该接口传递查询参数获取要更新的记录，并使用 body 中的对象进行记录合并更新
  result = api_instance.record_update_by_query(bodyqueryappindex, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecordsApi->record_update_by_query: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Object**](Object.md)| 要更新的字段和对应的新字段值，只支持最基本类型 | 
 **query** | **Integer**| 查询编号 | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **params** | [**Hash&lt;String, String&gt;**](String.md)| 查询参数 | [optional] 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **record_upload**
> Object record_upload(appindex, opts)

上传记录

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecordsApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
opts = { 
  files: ['files_example'] # Array<String> | 
  combine: false # BOOLEAN | 更新策略：合并还是替换，combine=true 为合并模式
  use_id_as_id_value: true # BOOLEAN | 使用数据中的 id 值作为记录 _id, 如果没有 id 字段则自动生成
}

begin
  #上传记录
  result = api_instance.record_upload(appindex, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecordsApi->record_upload: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **files** | [**Array&lt;String&gt;**](String.md)|  | [optional] 
 **combine** | **BOOLEAN**| 更新策略：合并还是替换，combine&#x3D;true 为合并模式 | [optional] [default to false]
 **use_id_as_id_value** | **BOOLEAN**| 使用数据中的 id 值作为记录 _id, 如果没有 id 字段则自动生成 | [optional] [default to true]

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json



# **record_upload_0**
> Object record_upload_0(appindex, opts)

上传记录，该接口已经废弃，请使用 /records/{app}/{index}/batch 接口

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecordsApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
opts = { 
  files: ['files_example'] # Array<String> | 
  combine: false # BOOLEAN | 更新策略：合并还是替换，combine=true 为合并模式
  use_id_as_id_value: true # BOOLEAN | 使用数据中的 id 值作为记录 _id, 如果没有 id 字段则自动生成
}

begin
  #上传记录，该接口已经废弃，请使用 /records/{app}/{index}/batch 接口
  result = api_instance.record_upload_0(appindex, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecordsApi->record_upload_0: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **files** | [**Array&lt;String&gt;**](String.md)|  | [optional] 
 **combine** | **BOOLEAN**| 更新策略：合并还是替换，combine&#x3D;true 为合并模式 | [optional] [default to false]
 **use_id_as_id_value** | **BOOLEAN**| 使用数据中的 id 值作为记录 _id, 如果没有 id 字段则自动生成 | [optional] [default to true]

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json



