# IndexeaClient::QuerySortField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **String** | 排序字段名 | [optional] 
**label** | **String** | 排序字段在前端显示的名称 | [optional] 
**order** | **String** | 默认排序方式 | [optional] [default to &#x27;desc&#x27;]

