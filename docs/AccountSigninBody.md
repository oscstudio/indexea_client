# IndexeaClient::AccountSigninBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **String** |  | 
**pwd** | **String** |  | 
**keep_login** | **BOOLEAN** |  | [optional] [default to false]

