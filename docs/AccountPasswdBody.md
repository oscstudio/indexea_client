# IndexeaClient::AccountPasswdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **String** | 旧密码 | 
**new_password** | **String** | 新密码 | 

