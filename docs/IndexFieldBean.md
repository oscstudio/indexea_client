# IndexeaClient::IndexFieldBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | [**IndexBean**](IndexBean.md) |  | [optional] 
**fields** | **Object** | 字段列表信息 | [optional] 

