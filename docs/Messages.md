# IndexeaClient::Messages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  | [optional] 
**msgs** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 

