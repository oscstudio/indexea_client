# IndexeaClient::QueryVariableBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**name** | **String** | 变量名 | [optional] 
**pname** | **String** | 变量对应 HTTP 请求的参数名 | [optional] 
**intro** | **String** |  | [optional] 
**type** | **String** | 变量类型 | [optional] 
**required** | **BOOLEAN** |  | [optional] 
**format** | **String** |  | [optional] 
**value** | **String** |  | [optional] 
**values** | **String** | 变量可用值列表，以逗号分隔 | [optional] 
**created_at** | **DateTime** |  | [optional] 

