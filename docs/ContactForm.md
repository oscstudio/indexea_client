# IndexeaClient::ContactForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **String** |  | 
**name** | **String** |  | 
**tel** | **String** |  | 
**email** | **String** |  | [optional] 
**memo** | **String** |  | [optional] 

