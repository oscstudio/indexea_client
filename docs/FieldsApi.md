# IndexeaClient::FieldsApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**index_fields**](FieldsApi.md#index_fields) | **GET** /indices/{app}/{index}/fields | 获取索引字段映射详情
[**index_update_fields**](FieldsApi.md#index_update_fields) | **POST** /indices/{app}/{index}/fields | 更新索引的字段映射
[**index_update_html_strip_fields**](FieldsApi.md#index_update_html_strip_fields) | **PATCH** /indices/{app}/{index}/fields | 更新索引的HTML过滤字段列表
[**index_values_of_field**](FieldsApi.md#index_values_of_field) | **GET** /indices/{app}/{index}/fields/{field} | 获取索引字段的所有值列表

# **index_fields**
> Object index_fields(app, index)

获取索引字段映射详情

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::FieldsApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #获取索引字段映射详情
  result = api_instance.index_fields(app, index)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling FieldsApi->index_fields: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_update_fields**
> Object index_update_fields(bodyappindex)

更新索引的字段映射

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::FieldsApi.new
body = nil # Object | 
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #更新索引的字段映射
  result = api_instance.index_update_fields(bodyappindex)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling FieldsApi->index_update_fields: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Object**](Object.md)|  | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **index_update_html_strip_fields**
> Object index_update_html_strip_fields(bodyappindex)

更新索引的HTML过滤字段列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::FieldsApi.new
body = nil # Object | 
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #更新索引的HTML过滤字段列表
  result = api_instance.index_update_html_strip_fields(bodyappindex)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling FieldsApi->index_update_html_strip_fields: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Object**](Object.md)|  | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **index_values_of_field**
> Array&lt;ValueOfField&gt; index_values_of_field(app, index, field, size)

获取索引字段的所有值列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::FieldsApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
field = 'field_example' # String | 字段名称
size = 20 # Integer | values count


begin
  #获取索引字段的所有值列表
  result = api_instance.index_values_of_field(app, index, field, size)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling FieldsApi->index_values_of_field: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **field** | **String**| 字段名称 | 
 **size** | **Integer**| values count | [default to 20]

### Return type

[**Array&lt;ValueOfField&gt;**](ValueOfField.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



