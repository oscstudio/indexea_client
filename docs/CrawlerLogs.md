# IndexeaClient::CrawlerLogs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  | [optional] 
**logs** | [**Array&lt;CrawlerLog&gt;**](CrawlerLog.md) |  | [optional] 

