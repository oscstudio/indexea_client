# IndexeaClient::RecommendBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** | 编号 | [optional] 
**ident** | **String** | 推荐标识 | [optional] 
**app** | **Integer** | 应用编号 | [optional] 
**account** | **Integer** | 账号 | [optional] 
**index** | **Integer** | 关联的索引编号 | [optional] 
**name** | **String** | 名称 | [optional] 
**intro** | **String** | 简介 | [optional] 
**type** | **String** | 类型 | [optional] 
**settings** | **Object** | 推荐设定 | [optional] 
**status** | **Integer** | 状态 | [optional] 
**created_at** | **DateTime** | 创建时间 | [optional] 
**updated_at** | **DateTime** | 最后更新时间 | [optional] 

