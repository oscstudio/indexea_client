# IndexeaClient::AccountsMessageBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**receiver** | **Integer** |  | 
**msg** | **String** |  | 

