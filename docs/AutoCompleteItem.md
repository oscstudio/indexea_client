# IndexeaClient::AutoCompleteItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**props** | **Object** |  | [optional] 

