# IndexeaClient::AccountBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**scopes** | **String** | 如果是第三方应用，此值为三方应用的权限 | [optional] 
**token** | **String** | 登录令牌(执行登录方法时该值为可用的 token) | [optional] 
**token_at** | **DateTime** | token 的有效时间 | [optional] 
**name** | **String** |  | [optional] 
**account** | **String** |  | [optional] 
**avatar** | **String** |  | [optional] 
**company** | **String** |  | [optional] 
**nation** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**address** | **String** |  | [optional] 
**login_ip** | **String** |  | [optional] 
**login_at** | **DateTime** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**origin** | **String** | 账号来源 | [optional] 
**subscription** | **Integer** |  | [optional] 
**settings** | **Object** |  | [optional] 
**roles** | **Array&lt;String&gt;** |  | [optional] 
**status** | **Integer** |  | [optional] 
**apps** | [**Array&lt;AppBean&gt;**](AppBean.md) | 应用列表 | [optional] 

