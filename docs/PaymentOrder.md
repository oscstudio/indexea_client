# IndexeaClient::PaymentOrder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**type** | **Integer** | 订单类型：1新购订单,2存续费订单,3纯扩容订单，4续费+扩容订单 | [optional] 
**account** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**ident** | **String** | 订单号 | [optional] 
**storage** | **Integer** | 存储空间 | [optional] 
**shards** | **Integer** | 分片数 | [optional] 
**replicas** | **Integer** | 副本数 | [optional] 
**searchs** | **Integer** | 搜索次数 | [optional] 
**days** | **Integer** | 购买的天数 | [optional] 
**price** | **Integer** |  | [optional] 
**payment** | **String** |  | [optional] 
**remark** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**expired_at** | **DateTime** |  | [optional] 
**receipt** | **Integer** | 订单的银行回执 | [optional] 
**receipt_url** | **String** | 银行回执的图片地址 | [optional] 
**status** | **Integer** |  | [optional] 

