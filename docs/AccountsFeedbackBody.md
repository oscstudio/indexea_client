# IndexeaClient::AccountsFeedbackBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **String** |  | 
**type** | **String** |  | [default to &#x27;general&#x27;]

