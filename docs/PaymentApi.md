# IndexeaClient::PaymentApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**payment_alipay**](PaymentApi.md#payment_alipay) | **POST** /payment/alipay | 接受支付宝的支付结果
[**payment_alipay_return**](PaymentApi.md#payment_alipay_return) | **GET** /payment/alipay | 支付宝平台支付完毕后调整到该接口
[**payment_apply_invoice**](PaymentApi.md#payment_apply_invoice) | **POST** /payment/{app}/invoices | 申请发票
[**payment_begin_pay**](PaymentApi.md#payment_begin_pay) | **POST** /payment/{app}/orders/{ident} | 支付订单
[**payment_buy**](PaymentApi.md#payment_buy) | **PUT** /payment/{app}/orders | 下单购买
[**payment_delete_invoice**](PaymentApi.md#payment_delete_invoice) | **DELETE** /payment/{app}/invoices | 删除发票
[**payment_delete_order**](PaymentApi.md#payment_delete_order) | **DELETE** /payment/{app}/orders/{ident} | 取消订单
[**payment_invoices**](PaymentApi.md#payment_invoices) | **GET** /payment/{app}/invoices | 获取发票列表
[**payment_order**](PaymentApi.md#payment_order) | **GET** /payment/{app}/orders/{ident} | 获取订单详情
[**payment_orders**](PaymentApi.md#payment_orders) | **POST** /payment/{app}/orders | 订单列表
[**payment_orders_without_invoice**](PaymentApi.md#payment_orders_without_invoice) | **GET** /payment/{app}/orders_without_invoice | 获取未曾开票的订单列表
[**payment_price**](PaymentApi.md#payment_price) | **GET** /payment/{app}/price | 获取套餐价格
[**payment_receipt**](PaymentApi.md#payment_receipt) | **GET** /payment/{app}/orders | 获取订单回执图片
[**payment_request_contact**](PaymentApi.md#payment_request_contact) | **POST** /payment/{app}/contact | 联系销售获取私有化报价
[**payment_upload_receipt**](PaymentApi.md#payment_upload_receipt) | **PUT** /payment/{app}/orders/{ident} | 上传转账回执
[**payment_wepay**](PaymentApi.md#payment_wepay) | **POST** /payment/wepay | 接受微信支付的支付结果

# **payment_alipay**
> payment_alipay

接受支付宝的支付结果

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::PaymentApi.new

begin
  #接受支付宝的支付结果
  api_instance.payment_alipay
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_alipay: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **payment_alipay_return**
> payment_alipay_return

支付宝平台支付完毕后调整到该接口

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::PaymentApi.new

begin
  #支付宝平台支付完毕后调整到该接口
  api_instance.payment_alipay_return
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_alipay_return: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



# **payment_apply_invoice**
> PaymentInvoice payment_apply_invoice(bodytypeapp)

申请发票

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
body = ['body_example'] # Array<String> | 申请开发票的订单编号列表
type = 1 # Integer | 发票类型
app = 'app_example' # String | 应用标识


begin
  #申请发票
  result = api_instance.payment_apply_invoice(bodytypeapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_apply_invoice: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Array&lt;String&gt;**](String.md)| 申请开发票的订单编号列表 | 
 **type** | **Integer**| 发票类型 | [default to 1]
 **app** | **String**| 应用标识 | 

### Return type

[**PaymentInvoice**](PaymentInvoice.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **payment_begin_pay**
> PayResult payment_begin_pay(app, ident, type)

支付订单

选择支付方式，开始支付

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
app = 'app_example' # String | 应用标识
ident = 'ident_example' # String | 订单号
type = 'type_example' # String | 支付方式


begin
  #支付订单
  result = api_instance.payment_begin_pay(app, ident, type)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_begin_pay: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **ident** | **String**| 订单号 | 
 **type** | **String**| 支付方式 | 

### Return type

[**PayResult**](PayResult.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **payment_buy**
> PaymentOrder payment_buy(bodyapp)

下单购买

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
body = IndexeaClient::PaymentService.new # PaymentService | 
app = 'app_example' # String | 应用标识


begin
  #下单购买
  result = api_instance.payment_buy(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_buy: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PaymentService**](PaymentService.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

[**PaymentOrder**](PaymentOrder.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **payment_delete_invoice**
> BOOLEAN payment_delete_invoice(app, id)

删除发票

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
app = 'app_example' # String | 应用标识
id = 56 # Integer | 发票编号


begin
  #删除发票
  result = api_instance.payment_delete_invoice(app, id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_delete_invoice: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **id** | **Integer**| 发票编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **payment_delete_order**
> BOOLEAN payment_delete_order(app, ident)

取消订单

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
app = 'app_example' # String | 应用标识
ident = 'ident_example' # String | 订单号


begin
  #取消订单
  result = api_instance.payment_delete_order(app, ident)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_delete_order: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **ident** | **String**| 订单号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **payment_invoices**
> Array&lt;PaymentInvoice&gt; payment_invoices(app)

获取发票列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
app = 'app_example' # String | 应用标识


begin
  #获取发票列表
  result = api_instance.payment_invoices(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_invoices: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**Array&lt;PaymentInvoice&gt;**](PaymentInvoice.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **payment_order**
> PaymentOrder payment_order(app, ident)

获取订单详情

获取订单详情

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
app = 'app_example' # String | 应用标识
ident = 'ident_example' # String | 订单号


begin
  #获取订单详情
  result = api_instance.payment_order(app, ident)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_order: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **ident** | **String**| 订单号 | 

### Return type

[**PaymentOrder**](PaymentOrder.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **payment_orders**
> Array&lt;PaymentOrder&gt; payment_orders(app)

订单列表

获取应用的订单列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
app = 'app_example' # String | 应用标识


begin
  #订单列表
  result = api_instance.payment_orders(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_orders: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**Array&lt;PaymentOrder&gt;**](PaymentOrder.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **payment_orders_without_invoice**
> Array&lt;PaymentOrder&gt; payment_orders_without_invoice(app)

获取未曾开票的订单列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
app = 'app_example' # String | 应用标识


begin
  #获取未曾开票的订单列表
  result = api_instance.payment_orders_without_invoice(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_orders_without_invoice: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**Array&lt;PaymentOrder&gt;**](PaymentOrder.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **payment_price**
> PaymentService payment_price(app, service)

获取套餐价格

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
app = 'app_example' # String | 应用标识
service = IndexeaClient::PaymentService.new # PaymentService | 配额信息


begin
  #获取套餐价格
  result = api_instance.payment_price(app, service)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_price: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **service** | [**PaymentService**](.md)| 配额信息 | 

### Return type

[**PaymentService**](PaymentService.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **payment_receipt**
> String payment_receipt(app, ident, id)

获取订单回执图片

查看回执图片

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
app = 'app_example' # String | 应用标识
ident = 'ident_example' # String | 订单号
id = 56 # Integer | 回执编号


begin
  #获取订单回执图片
  result = api_instance.payment_receipt(app, ident, id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_receipt: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **ident** | **String**| 订单号 | 
 **id** | **Integer**| 回执编号 | 

### Return type

**String**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: default



# **payment_request_contact**
> BOOLEAN payment_request_contact(bodyapp)

联系销售获取私有化报价

获取私有化方案、报价等信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
body = IndexeaClient::ContactForm.new # ContactForm | 客户联系信息
app = 'app_example' # String | 应用标识


begin
  #联系销售获取私有化报价
  result = api_instance.payment_request_contact(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_request_contact: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ContactForm**](ContactForm.md)| 客户联系信息 | 
 **app** | **String**| 应用标识 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **payment_upload_receipt**
> PaymentOrder payment_upload_receipt(receiptappident)

上传转账回执

用户在完成银行转账后，通过该接口上传转账回执

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::PaymentApi.new
receipt = 'receipt_example' # String | 
app = 'app_example' # String | 应用标识
ident = 'ident_example' # String | 订单号


begin
  #上传转账回执
  result = api_instance.payment_upload_receipt(receiptappident)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_upload_receipt: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receipt** | **String**|  | 
 **app** | **String**| 应用标识 | 
 **ident** | **String**| 订单号 | 

### Return type

[**PaymentOrder**](PaymentOrder.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json



# **payment_wepay**
> payment_wepay

接受微信支付的支付结果

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::PaymentApi.new

begin
  #接受微信支付的支付结果
  api_instance.payment_wepay
rescue IndexeaClient::ApiError => e
  puts "Exception when calling PaymentApi->payment_wepay: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



