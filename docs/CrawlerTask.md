# IndexeaClient::CrawlerTask

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **Integer** |  | [optional] 
**template** | **String** |  | [optional] 
**period** | **Integer** |  | [optional] 
**settings** | **Object** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**run_times** | **Integer** |  | [optional] 
**last_updated_at** | **DateTime** |  | [optional] 
**last_error** | **String** |  | [optional] 
**error_count** | **Integer** |  | [optional] 
**status** | **Integer** |  | [optional] 

