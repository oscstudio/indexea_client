# IndexeaClient::GlobalApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**json**](GlobalApi.md#json) | **GET** /json | 接口定义(JSON)
[**options_get**](GlobalApi.md#options_get) | **GET** /options | 系统全局配置接口
[**status_database**](GlobalApi.md#status_database) | **GET** /status/database | 数据库服务状态测试
[**status_engine**](GlobalApi.md#status_engine) | **GET** /status/engine | 搜索引擎状态测试
[**welcome**](GlobalApi.md#welcome) | **GET** / | 接口欢迎信息
[**yaml**](GlobalApi.md#yaml) | **GET** /yaml | 接口定义(YAML)

# **json**
> Object json

接口定义(JSON)

获取 OpenAPI 接口定义(JSON)

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::GlobalApi.new

begin
  #接口定义(JSON)
  result = api_instance.json
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling GlobalApi->json: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **options_get**
> Object options_get(name, keys)

系统全局配置接口

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::GlobalApi.new
name = 'name_example' # String | 
keys = 'keys_example' # String | 配置项名,多个配置项请使用逗号隔开


begin
  #系统全局配置接口
  result = api_instance.options_get(name, keys)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling GlobalApi->options_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  | 
 **keys** | **String**| 配置项名,多个配置项请使用逗号隔开 | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **status_database**
> String status_database

数据库服务状态测试

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::GlobalApi.new

begin
  #数据库服务状态测试
  result = api_instance.status_database
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling GlobalApi->status_database: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain



# **status_engine**
> String status_engine

搜索引擎状态测试

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::GlobalApi.new

begin
  #搜索引擎状态测试
  result = api_instance.status_engine
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling GlobalApi->status_engine: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain



# **welcome**
> String welcome

接口欢迎信息

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::GlobalApi.new

begin
  #接口欢迎信息
  result = api_instance.welcome
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling GlobalApi->welcome: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, text/html, application/json, application/x-yaml



# **yaml**
> String yaml

接口定义(YAML)

获取 OpenAPI 接口定义(YAML)

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::GlobalApi.new

begin
  #接口定义(YAML)
  result = api_instance.yaml
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling GlobalApi->yaml: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/x-yaml



