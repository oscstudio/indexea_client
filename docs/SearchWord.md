# IndexeaClient::SearchWord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**q** | **String** | 搜索词 | [optional] 
**count** | **Integer** | 该词搜索次数 | [optional] 

