# IndexeaClient::Bulletin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**level** | **String** |  | [optional] 
**scope** | **Integer** |  | [optional] 
**title** | **String** |  | [optional] 
**content** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**start_at** | **DateTime** |  | [optional] 
**expired_at** | **DateTime** |  | [optional] 
**closable** | **BOOLEAN** |  | [optional] 

