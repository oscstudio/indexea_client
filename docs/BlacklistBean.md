# IndexeaClient::BlacklistBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | **Integer** |  | [optional] 
**block_type** | **Integer** |  | [optional] 
**badwords** | **Array&lt;String&gt;** |  | [optional] 
**black_ips** | **Array&lt;String&gt;** |  | [optional] 
**white_ips** | **Array&lt;String&gt;** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 

