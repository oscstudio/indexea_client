# IndexeaClient::WidgetStatusForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Integer** |  | 
**options** | [**Array&lt;OptionForm&gt;**](OptionForm.md) |  | [optional] 

