# IndexeaClient::WidgetForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**type** | **String** |  | [default to &#x27;web&#x27;]
**sub_type** | **String** |  | [optional] [default to &#x27;None&#x27;]
**intro** | **String** |  | [optional] 
**queries** | **Array&lt;Integer&gt;** |  | 
**layout** | **String** |  | [optional] 
**status** | **Integer** |  | [optional] 

