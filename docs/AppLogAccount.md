# IndexeaClient::AppLogAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**avatar** | **String** |  | [optional] 
**name** | **String** |  | [optional] 

