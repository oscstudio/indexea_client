# IndexeaClient::PaymentInvoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**account** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**orders** | [**Array&lt;PaymentOrder&gt;**](PaymentOrder.md) |  | [optional] 
**price** | **Integer** |  | [optional] 
**type** | **Integer** |  | [optional] 
**status** | **Integer** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**logs** | **Array&lt;String&gt;** |  | [optional] 

