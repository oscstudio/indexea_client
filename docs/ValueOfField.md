# IndexeaClient::ValueOfField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | 字段值 | [optional] 
**doc_count** | **Integer** | 记录数 | [optional] 

