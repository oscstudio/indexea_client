# IndexeaClient::SearchApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**search_click**](SearchApi.md#search_click) | **POST** /search/widget/{widget}/click | 搜索结果点击行为收集
[**search_histories**](SearchApi.md#search_histories) | **GET** /search/widget/{widget}/histories | 获取当前搜索用户的最新搜索记录
[**search_logs**](SearchApi.md#search_logs) | **GET** /apps/{app}/logs-searchs | 获取搜索日志
[**search_query_histories**](SearchApi.md#search_query_histories) | **GET** /search/query/{query}/histories | 获取当前搜索用户的最新搜索记录
[**search_query_hot_words**](SearchApi.md#search_query_hot_words) | **GET** /search/query/{query}/hotwords | 获取查询相关热词
[**search_query_repeat_scroll**](SearchApi.md#search_query_repeat_scroll) | **POST** /search/scroll/{query} | 读取滚动搜索结果
[**search_query_scroll**](SearchApi.md#search_query_scroll) | **GET** /search/scroll/{query} | 基于查询的滚动搜索
[**search_query_search**](SearchApi.md#search_query_search) | **GET** /search/query/{query} | 基于查询的公开搜索
[**search_widget_auto_complete**](SearchApi.md#search_widget_auto_complete) | **GET** /search/widget/{widget}/autocomplete | 基于组件的搜索词自动完成
[**search_widget_hot_words**](SearchApi.md#search_widget_hot_words) | **GET** /search/widget/{widget}/hotwords | 获取组件搜索的相关热词
[**search_widget_search**](SearchApi.md#search_widget_search) | **GET** /search/widget/{widget} | 基于组件的公开搜索

# **search_click**
> BOOLEAN search_click(widget, action_id, doc_id, opts)

搜索结果点击行为收集

该接口主要用于记录用户对搜索结果的点击行为

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::SearchApi.new
widget = 'widget_example' # String | 组件唯一标识
action_id = 'action_id_example' # String | 对应搜索行为编号
doc_id = 'doc_id_example' # String | 对应索引中的内部记录编号
opts = { 
  userid: 'userid_example', # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
  x_token: 'x_token_example' # String | 如果要使用非发布的组件，需要组件作者授权
}

begin
  #搜索结果点击行为收集
  result = api_instance.search_click(widget, action_id, doc_id, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_click: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **widget** | **String**| 组件唯一标识 | 
 **action_id** | **String**| 对应搜索行为编号 | 
 **doc_id** | **String**| 对应索引中的内部记录编号 | 
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 

### Return type

**BOOLEAN**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **search_histories**
> Array&lt;String&gt; search_histories(widget, strategy, size, opts)

获取当前搜索用户的最新搜索记录

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::SearchApi.new
widget = 'widget_example' # String | 组件唯一标识
strategy = 'popular' # String | 搜索记录策略
size = 10 # Integer | 数量
opts = { 
  userid: 'userid_example', # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
  x_token: 'x_token_example', # String | 如果要使用非公开的组件，需要组件作者授权
  query: 0 # Integer | 指定关联查询的编号
}

begin
  #获取当前搜索用户的最新搜索记录
  result = api_instance.search_histories(widget, strategy, size, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_histories: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **widget** | **String**| 组件唯一标识 | 
 **strategy** | **String**| 搜索记录策略 | [default to popular]
 **size** | **Integer**| 数量 | [default to 10]
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 
 **x_token** | **String**| 如果要使用非公开的组件，需要组件作者授权 | [optional] 
 **query** | **Integer**| 指定关联查询的编号 | [optional] [default to 0]

### Return type

**Array&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **search_logs**
> Array&lt;QueryActionBean&gt; search_logs(app, opts)

获取搜索日志

该接口主要用于获取搜索明细

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::SearchApi.new
app = 'app_example' # String | 应用标识
opts = { 
  indices: [56], # Array<Integer> | 只看指定索引
  scope: 'all', # String | 搜索范围
  widget: 0, # Integer | 搜索组件
  query: 0, # Integer | 指定查询
  recomm: 0, # Integer | 推荐组件
  start_date: Date.parse('2013-10-20'), # Date | 统计起始日期
  end_date: Date.parse('2013-10-20'), # Date | 统计结束日期
  from: 0, # Integer | 起始位置
  size: 50 # Integer | 每页记录数量
}

begin
  #获取搜索日志
  result = api_instance.search_logs(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_logs: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **indices** | [**Array&lt;Integer&gt;**](Integer.md)| 只看指定索引 | [optional] 
 **scope** | **String**| 搜索范围 | [optional] [default to all]
 **widget** | **Integer**| 搜索组件 | [optional] [default to 0]
 **query** | **Integer**| 指定查询 | [optional] [default to 0]
 **recomm** | **Integer**| 推荐组件 | [optional] [default to 0]
 **start_date** | **Date**| 统计起始日期 | [optional] 
 **end_date** | **Date**| 统计结束日期 | [optional] 
 **from** | **Integer**| 起始位置 | [optional] [default to 0]
 **size** | **Integer**| 每页记录数量 | [optional] [default to 50]

### Return type

[**Array&lt;QueryActionBean&gt;**](QueryActionBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **search_query_histories**
> Array&lt;String&gt; search_query_histories(query, strategy, size, opts)

获取当前搜索用户的最新搜索记录

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::SearchApi.new
query = 'query_example' # String | 查询唯一标识
strategy = 'popular' # String | 搜索记录策略
size = 10 # Integer | 数量
opts = { 
  userid: 'userid_example', # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
  x_token: 'x_token_example' # String | 如果要使用非公开查询，需要组件作者授权
}

begin
  #获取当前搜索用户的最新搜索记录
  result = api_instance.search_query_histories(query, strategy, size, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_query_histories: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **String**| 查询唯一标识 | 
 **strategy** | **String**| 搜索记录策略 | [default to popular]
 **size** | **Integer**| 数量 | [default to 10]
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 
 **x_token** | **String**| 如果要使用非公开查询，需要组件作者授权 | [optional] 

### Return type

**Array&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **search_query_hot_words**
> Array&lt;SearchWord&gt; search_query_hot_words(query, scope, count, opts)

获取查询相关热词

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::SearchApi.new
query = 'query_example' # String | 查询标识
scope = 'all' # String | 时间范围
count = 10 # Integer | 获取热词数量
opts = { 
  x_token: 'x_token_example', # String | 如果要使用非发布的组件，需要组件作者授权
  userid: 'userid_example' # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
}

begin
  #获取查询相关热词
  result = api_instance.search_query_hot_words(query, scope, count, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_query_hot_words: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **String**| 查询标识 | 
 **scope** | **String**| 时间范围 | [default to all]
 **count** | **Integer**| 获取热词数量 | [default to 10]
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 

### Return type

[**Array&lt;SearchWord&gt;**](SearchWord.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **search_query_repeat_scroll**
> Object search_query_repeat_scroll(query, id, opts)

读取滚动搜索结果

先调用 search.queryScroll 获取 scroll_id 值以及第一批结果记录，然后使用 scroll_id 值调用该接口获取下一批结果记录，请注意该值的有效期是 1 分钟

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::SearchApi.new
query = 'query_example' # String | 查询标识
id = 'id_example' # String | scroll_id 值，该值的有效期是 1 分钟
opts = { 
  x_token: 'x_token_example', # String | 如果要使用非发布的组件，需要组件作者授权
  userid: 'userid_example' # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
}

begin
  #读取滚动搜索结果
  result = api_instance.search_query_repeat_scroll(query, id, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_query_repeat_scroll: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **String**| 查询标识 | 
 **id** | **String**| scroll_id 值，该值的有效期是 1 分钟 | 
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **search_query_scroll**
> Object search_query_scroll(query, opts)

基于查询的滚动搜索

用于读取超过 10000 条记录的搜索结果，当需要读取大批量查询结果时请采用此接口获得 scroll_id 值，然后再用 POST 方法 queryRepeatScroll 读取剩余的其他结果，每批次的读取间隔不能超过 1 分钟

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::SearchApi.new
query = 'query_example' # String | 查询标识
opts = { 
  x_token: 'x_token_example', # String | 如果要使用非发布的组件，需要组件作者授权
  userid: 'userid_example', # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
  size: 100, # Integer | 单次滚动的记录数
  q: 'q_example', # String | 搜索关键字
  params: {'key' => 'params_example'} # Hash<String, String> | 聚合参数
}

begin
  #基于查询的滚动搜索
  result = api_instance.search_query_scroll(query, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_query_scroll: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **String**| 查询标识 | 
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 
 **size** | **Integer**| 单次滚动的记录数 | [optional] [default to 100]
 **q** | **String**| 搜索关键字 | [optional] 
 **params** | [**Hash&lt;String, String&gt;**](String.md)| 聚合参数 | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **search_query_search**
> Object search_query_search(query, from, size, opts)

基于查询的公开搜索

该接口主要用于公开搜索，如果查询是公开的就不需要授权

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::SearchApi.new
query = 'query_example' # String | 查询标识
from = 0 # Integer | 起始记录
size = 20 # Integer | 每页记录数量
opts = { 
  x_token: 'x_token_example', # String | 如果要使用非发布的组件，需要组件作者授权
  userid: 'userid_example', # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
  q: 'q_example', # String | 搜索关键字
  params: {'key' => 'params_example'} # Hash<String, String> | 聚合参数
}

begin
  #基于查询的公开搜索
  result = api_instance.search_query_search(query, from, size, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_query_search: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **String**| 查询标识 | 
 **from** | **Integer**| 起始记录 | [default to 0]
 **size** | **Integer**| 每页记录数量 | [default to 20]
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 
 **q** | **String**| 搜索关键字 | [optional] 
 **params** | [**Hash&lt;String, String&gt;**](String.md)| 聚合参数 | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **search_widget_auto_complete**
> Array&lt;AutoCompleteItem&gt; search_widget_auto_complete(widget, q, size, opts)

基于组件的搜索词自动完成

该接口主要为搜索输入框提供自动完成的功能

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::SearchApi.new
widget = 'widget_example' # String | 组件唯一标识
q = 'q_example' # String | 搜索关键字
size = 10 # Integer | 数量
opts = { 
  userid: 'userid_example', # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
  x_token: 'x_token_example', # String | 如果要使用非发布的组件，需要组件作者授权
  query: 0 # Integer | 查询编号
}

begin
  #基于组件的搜索词自动完成
  result = api_instance.search_widget_auto_complete(widget, q, size, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_widget_auto_complete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **widget** | **String**| 组件唯一标识 | 
 **q** | **String**| 搜索关键字 | 
 **size** | **Integer**| 数量 | [default to 10]
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 
 **query** | **Integer**| 查询编号 | [optional] [default to 0]

### Return type

[**Array&lt;AutoCompleteItem&gt;**](AutoCompleteItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **search_widget_hot_words**
> Array&lt;SearchWord&gt; search_widget_hot_words(widget, opts)

获取组件搜索的相关热词

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::SearchApi.new
widget = 'widget_example' # String | 组件唯一标识
opts = { 
  x_token: 'x_token_example', # String | 如果要使用非发布的组件，需要组件作者授权
  userid: 'userid_example', # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
  query: 56, # Integer | 查询编号
  scope: 'scope_example', # String | 时间范围
  count: 10 # Integer | 获取热词数量
}

begin
  #获取组件搜索的相关热词
  result = api_instance.search_widget_hot_words(widget, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_widget_hot_words: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **widget** | **String**| 组件唯一标识 | 
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 
 **query** | **Integer**| 查询编号 | [optional] 
 **scope** | **String**| 时间范围 | [optional] 
 **count** | **Integer**| 获取热词数量 | [optional] [default to 10]

### Return type

[**Array&lt;SearchWord&gt;**](SearchWord.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **search_widget_search**
> Object search_widget_search(widget, from, size, opts)

基于组件的公开搜索

该接口主要为UI组件提供公开搜索

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::SearchApi.new
widget = 'widget_example' # String | 组件唯一标识
from = 0 # Integer | 起始记录
size = -1 # Integer | 每页记录数量, 如果值小于0则使用预设值的记录数
opts = { 
  userid: 'userid_example', # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
  x_token: 'x_token_example', # String | 如果要使用非发布的组件，需要组件作者授权
  original: 'original_example', # String | 搜索动作的延续，在 Web 组件中一边输入即时搜索时，使用的是同一个 original，original 值等于第一个搜索动作产生结果中的 action 值
  query: 0, # Integer | 查询编号
  q: 'q_example', # String | 搜索关键字
  params: {'key' => 'params_example'} # Hash<String, String> | 聚合参数
}

begin
  #基于组件的公开搜索
  result = api_instance.search_widget_search(widget, from, size, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling SearchApi->search_widget_search: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **widget** | **String**| 组件唯一标识 | 
 **from** | **Integer**| 起始记录 | [default to 0]
 **size** | **Integer**| 每页记录数量, 如果值小于0则使用预设值的记录数 | [default to -1]
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 
 **original** | **String**| 搜索动作的延续，在 Web 组件中一边输入即时搜索时，使用的是同一个 original，original 值等于第一个搜索动作产生结果中的 action 值 | [optional] 
 **query** | **Integer**| 查询编号 | [optional] [default to 0]
 **q** | **String**| 搜索关键字 | [optional] 
 **params** | [**Hash&lt;String, String&gt;**](String.md)| 聚合参数 | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



