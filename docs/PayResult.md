# IndexeaClient::PayResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**props** | **Object** |  | [optional] 

