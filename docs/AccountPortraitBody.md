# IndexeaClient::AccountPortraitBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portrait** | **String** |  | [optional] 

