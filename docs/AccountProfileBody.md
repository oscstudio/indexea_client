# IndexeaClient::AccountProfileBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pwd** | **String** | 操作者的登录密码 | 
**vcode** | **String** | 操作验证码 | 

