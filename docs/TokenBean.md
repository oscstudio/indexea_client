# IndexeaClient::TokenBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**token** | **String** |  | [optional] 
**scopes** | **Object** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**expired_at** | **DateTime** |  | [optional] 

