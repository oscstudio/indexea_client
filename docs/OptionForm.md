# IndexeaClient::OptionForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | 配置项名称 | 
**type** | **String** | 配置值类型 | 
**value** | **String** | 配置值 | 
**vcode** | **String** | 验证码(非必须，只有在配置通知邮箱和手机的时候才需要) | [optional] 

