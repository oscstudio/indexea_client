# IndexeaClient::IntelligentMapping

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**word** | **String** | 关键词 | [optional] 
**field** | **String** | 匹配字段 | [optional] 

