# IndexeaClient::IndexBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**account** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**system** | **BOOLEAN** | 是否系统索引（系统索引不允许用户删除） | [optional] 
**intro** | **String** |  | [optional] 
**_alias** | **Array&lt;String&gt;** |  | [optional] 
**type** | **String** |  | [optional] 
**filters** | **Array&lt;Object&gt;** | 索引关联的临时过滤器 | [optional] 
**aggs** | **Object** | 索引关联的临时聚合器 | [optional] 
**sorts** | **Array&lt;Object&gt;** | 索引关联的临时排序 | [optional] 
**shards** | **Integer** | 该索引的分片数 | [optional] 
**replicas** | **Integer** | 该索引的副本数 | [optional] 
**analyzer** | **String** |  | [optional] 
**search_analyzer** | **String** |  | [optional] 
**stat** | [**IndexStatBean**](IndexStatBean.md) |  | [optional] 
**options** | **Object** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**status** | **Integer** |  | [optional] 

