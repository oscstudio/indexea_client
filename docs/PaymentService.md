# IndexeaClient::PaymentService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**days** | **Integer** | 服务天数 | [optional] 
**storage** | **Integer** | 存储空间 | [optional] 
**shards** | **Integer** | 分片数 | [optional] 
**replicas** | **Integer** | 副本数 | [optional] 
**searchs** | **Integer** | 搜索次数 | [optional] 
**price** | **Integer** | 服务价格 | [optional] 

