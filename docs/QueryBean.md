# IndexeaClient::QueryBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**ident** | **String** |  | [optional] 
**account** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**indices** | [**Array&lt;IndexBean&gt;**](IndexBean.md) | 关联的索引列表 | [optional] 
**name** | **String** |  | [optional] 
**intro** | **String** |  | [optional] 
**query** | **Object** |  | [optional] 
**suggest** | **Object** |  | [optional] 
**script_score** | **Object** |  | [optional] 
**script_fields** | **Object** |  | [optional] 
**sort_fields** | [**Array&lt;QuerySortField&gt;**](QuerySortField.md) |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**settings** | **Object** | 查询设置项 | [optional] 
**status** | **Integer** |  | [optional] 

