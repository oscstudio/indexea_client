# IndexeaClient::TriggerLogBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**log** | **Integer** |  | [optional] 
**url** | **String** |  | [optional] 
**body** | **String** |  | [optional] 
**resp_code** | **Integer** |  | [optional] 
**resp_body** | **String** |  | [optional] 
**resp_time** | **Integer** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 

