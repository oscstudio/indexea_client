# IndexeaClient::KeywordBindingBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**index** | **Integer** |  | [optional] 
**query** | **Integer** |  | [optional] 
**keyword** | **String** |  | [optional] 
**_alias** | **Array&lt;String&gt;** |  | [optional] 
**ids** | **Array&lt;String&gt;** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 

