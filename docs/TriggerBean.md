# IndexeaClient::TriggerBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app** | **Integer** |  | [optional] 
**url** | **String** |  | [optional] 
**triggers** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**all_triggers** | **Array&lt;String&gt;** |  | [optional] 
**enabled** | **BOOLEAN** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 

