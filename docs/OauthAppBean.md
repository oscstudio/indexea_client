# IndexeaClient::OauthAppBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**account** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**ident** | **String** | 三方应用标识 client id | [optional] 
**name** | **String** |  | [optional] 
**intro** | **String** |  | [optional] 
**logo** | **String** |  | [optional] 
**url** | **String** | 应用地址 | [optional] 
**secret** | **String** | 第三方应用密钥, 该值仅在创建和重置密钥时返回 | [optional] 
**callback** | **String** | 回调地址 | [optional] 
**scopes** | **String** | 授权范围 | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**status** | **Integer** |  | [optional] 

