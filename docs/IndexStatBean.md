# IndexeaClient::IndexStatBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **String** |  | [optional] 
**index** | **String** |  | [optional] 
**health** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**records** | **Integer** |  | [optional] 
**storage** | **Integer** |  | [optional] 
**shards** | **Integer** |  | [optional] 
**replicas** | **Integer** |  | [optional] 

