# IndexeaClient::QueriesApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**query_analyze**](QueriesApi.md#query_analyze) | **POST** /queries/{app}/analyze | 分词测试
[**query_copy**](QueriesApi.md#query_copy) | **POST** /queries/{app}/copy | 复制指定查询并创建新查询
[**query_copy_to_query**](QueriesApi.md#query_copy_to_query) | **PUT** /queries/{app}/copy | 复制查询到已有查询
[**query_create**](QueriesApi.md#query_create) | **POST** /queries/{app} | 创建搜索
[**query_create_keyword_bindings**](QueriesApi.md#query_create_keyword_bindings) | **POST** /queries/{app}/keyword-bindings | 创建新的关键词文档绑定
[**query_create_variable**](QueriesApi.md#query_create_variable) | **POST** /queries/{app}/variables | 创建新的预定义查询变量
[**query_delete**](QueriesApi.md#query_delete) | **DELETE** /queries/{app} | 删除搜索
[**query_delete_keyword_bindings**](QueriesApi.md#query_delete_keyword_bindings) | **DELETE** /queries/{app}/keyword-bindings | 删除关键词文档绑定
[**query_delete_node_positions**](QueriesApi.md#query_delete_node_positions) | **DELETE** /queries/{app}/node-positions | 清除查询条件的节点位置信息
[**query_delete_variable**](QueriesApi.md#query_delete_variable) | **DELETE** /queries/{app}/variables | 删除预定义查询变量
[**query_fields**](QueriesApi.md#query_fields) | **GET** /queries/{app}/fields | 获取查询关联的所有索引的字段信息
[**query_get**](QueriesApi.md#query_get) | **GET** /queries/{app}/{query} | 获取查询的详情
[**query_get_node_positions**](QueriesApi.md#query_get_node_positions) | **GET** /queries/{app}/node-positions | 获取查询条件的节点位置信息
[**query_get_record**](QueriesApi.md#query_get_record) | **GET** /queries/{app}/record | 获取记录的详情
[**query_keyword_bindings**](QueriesApi.md#query_keyword_bindings) | **GET** /queries/{app}/keyword-bindings | 获取查询的关键词文档绑定列表
[**query_list**](QueriesApi.md#query_list) | **GET** /queries/{app} | 获取应用下所有索引下的查询列表（按索引进行分组）
[**query_profile**](QueriesApi.md#query_profile) | **GET** /queries/{app}/profiler | 获取搜索诊断信息
[**query_records_of_keyword_binding**](QueriesApi.md#query_records_of_keyword_binding) | **GET** /queries/{app}/keyword-bindings-records | 获取关键词绑定对应的记录列表
[**query_save_intelligent_mappings**](QueriesApi.md#query_save_intelligent_mappings) | **PUT** /queries/{app}/intelligent-mappings | 设置索引智能匹配字段
[**query_save_node_positions**](QueriesApi.md#query_save_node_positions) | **PUT** /queries/{app}/node-positions | 保存查询条件的节点位置信息
[**query_search**](QueriesApi.md#query_search) | **GET** /queries/{app}/search | 搜索测试
[**query_source**](QueriesApi.md#query_source) | **POST** /queries/{app}/{query} | 获取最终查询的源码(JSON)
[**query_suggest**](QueriesApi.md#query_suggest) | **GET** /queries/{app}/suggest | 获取搜索建议列表
[**query_test_intelligent_mappings**](QueriesApi.md#query_test_intelligent_mappings) | **POST** /queries/{app}/intelligent-mappings | 测试索引智能匹配字段
[**query_update**](QueriesApi.md#query_update) | **PUT** /queries/{app} | 修改查询
[**query_update_keyword_bindings**](QueriesApi.md#query_update_keyword_bindings) | **PATCH** /queries/{app}/keyword-bindings | 修改关键词文档绑定
[**query_update_settings**](QueriesApi.md#query_update_settings) | **POST** /queries/{app}/settings | 更改查询的设置项
[**query_update_variable**](QueriesApi.md#query_update_variable) | **PATCH** /queries/{app}/variables | 修改预定义查询变量
[**query_validate**](QueriesApi.md#query_validate) | **GET** /queries/{app}/validate | 获取搜索验证结果
[**query_validate_aggregation**](QueriesApi.md#query_validate_aggregation) | **POST** /queries/{app}/validate-aggregation | 验证聚合定义是否正确
[**query_validate_query**](QueriesApi.md#query_validate_query) | **POST** /queries/{app}/validate-query | 验证聚合定义是否正确
[**query_validate_script_field**](QueriesApi.md#query_validate_script_field) | **POST** /queries/{app}/validate-script-field | 验证脚本字段是否正确
[**query_validate_script_score**](QueriesApi.md#query_validate_script_score) | **POST** /queries/{app}/validate-script-score | 验证脚本字段是否正确
[**query_validate_suggestion**](QueriesApi.md#query_validate_suggestion) | **POST** /queries/{app}/validate-suggest | 验证建议是否正确
[**query_variables**](QueriesApi.md#query_variables) | **GET** /queries/{app}/variables | 获取应用的预定义查询变量列表

# **query_analyze**
> Array&lt;AnalyzeToken&gt; query_analyze(bodyapp, opts)

分词测试

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = IndexeaClient::AnalyzeObject.new # AnalyzeObject | 
app = 'app_example' # String | 应用标识
opts = { 
  index: IndexeaClient::BigDecimal.new # BigDecimal | 索引编号，如果指定索引编号则使用索引的分词器
}

begin
  #分词测试
  result = api_instance.query_analyze(bodyapp, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_analyze: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AnalyzeObject**](AnalyzeObject.md)|  | 
 **app** | **String**| 应用标识 | 
 **index** | **BigDecimal**| 索引编号，如果指定索引编号则使用索引的分词器 | [optional] [default to 0]

### Return type

[**Array&lt;AnalyzeToken&gt;**](AnalyzeToken.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_copy**
> QueryBean query_copy(app, query)

复制指定查询并创建新查询

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 源查询编号


begin
  #复制指定查询并创建新查询
  result = api_instance.query_copy(app, query)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_copy: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 源查询编号 | 

### Return type

[**QueryBean**](QueryBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_copy_to_query**
> BOOLEAN query_copy_to_query(app, query, to)

复制查询到已有查询

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 源查询编号
to = 56 # Integer | 目标查询编号


begin
  #复制查询到已有查询
  result = api_instance.query_copy_to_query(app, query, to)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_copy_to_query: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 源查询编号 | 
 **to** | **Integer**| 目标查询编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_create**
> QueryBean query_create(bodyapp)

创建搜索

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = IndexeaClient::QueryForm.new # QueryForm | 
app = 'app_example' # String | 应用标识


begin
  #创建搜索
  result = api_instance.query_create(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**QueryForm**](QueryForm.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

[**QueryBean**](QueryBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_create_keyword_bindings**
> KeywordBindingBean query_create_keyword_bindings(bodyqueryapp)

创建新的关键词文档绑定

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = IndexeaClient::KeywordBindingBean.new # KeywordBindingBean | 
query = 56 # Integer | 查询编号
app = 'app_example' # String | 应用标识


begin
  #创建新的关键词文档绑定
  result = api_instance.query_create_keyword_bindings(bodyqueryapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_create_keyword_bindings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**KeywordBindingBean**](KeywordBindingBean.md)|  | 
 **query** | **Integer**| 查询编号 | 
 **app** | **String**| 应用标识 | 

### Return type

[**KeywordBindingBean**](KeywordBindingBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_create_variable**
> QueryVariableBean query_create_variable(bodyapp)

创建新的预定义查询变量

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = IndexeaClient::QueryVariableBean.new # QueryVariableBean | 
app = 'app_example' # String | 应用标识


begin
  #创建新的预定义查询变量
  result = api_instance.query_create_variable(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_create_variable: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**QueryVariableBean**](QueryVariableBean.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

[**QueryVariableBean**](QueryVariableBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_delete**
> BOOLEAN query_delete(app, query)

删除搜索

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号


begin
  #删除搜索
  result = api_instance.query_delete(app, query)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_delete_keyword_bindings**
> BOOLEAN query_delete_keyword_bindings(app, query, id)

删除关键词文档绑定

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号
id = 56 # Integer | 关键词编号


begin
  #删除关键词文档绑定
  result = api_instance.query_delete_keyword_bindings(app, query, id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_delete_keyword_bindings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 
 **id** | **Integer**| 关键词编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_delete_node_positions**
> BOOLEAN query_delete_node_positions(app, query)

清除查询条件的节点位置信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号


begin
  #清除查询条件的节点位置信息
  result = api_instance.query_delete_node_positions(app, query)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_delete_node_positions: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_delete_variable**
> BOOLEAN query_delete_variable(app, id)

删除预定义查询变量

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
id = 56 # Integer | 自定义查询变量编号


begin
  #删除预定义查询变量
  result = api_instance.query_delete_variable(app, id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_delete_variable: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **id** | **Integer**| 自定义查询变量编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_fields**
> Array&lt;IndexFieldBean&gt; query_fields(app, query)

获取查询关联的所有索引的字段信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号


begin
  #获取查询关联的所有索引的字段信息
  result = api_instance.query_fields(app, query)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_fields: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 

### Return type

[**Array&lt;IndexFieldBean&gt;**](IndexFieldBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_get**
> QueryBean query_get(app, query)

获取查询的详情

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号


begin
  #获取查询的详情
  result = api_instance.query_get(app, query)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 

### Return type

[**QueryBean**](QueryBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_get_node_positions**
> Object query_get_node_positions(app, query)

获取查询条件的节点位置信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号


begin
  #获取查询条件的节点位置信息
  result = api_instance.query_get_node_positions(app, query)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_get_node_positions: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_get_record**
> Object query_get_record(app, query, _id)

获取记录的详情

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号
_id = '_id_example' # String | 记录 _id 值


begin
  #获取记录的详情
  result = api_instance.query_get_record(app, query, _id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_get_record: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 
 **_id** | **String**| 记录 _id 值 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_keyword_bindings**
> Array&lt;KeywordBindingBean&gt; query_keyword_bindings(app, query)

获取查询的关键词文档绑定列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号


begin
  #获取查询的关键词文档绑定列表
  result = api_instance.query_keyword_bindings(app, query)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_keyword_bindings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 

### Return type

[**Array&lt;KeywordBindingBean&gt;**](KeywordBindingBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_list**
> Array&lt;QueryBean&gt; query_list(app, opts)

获取应用下所有索引下的查询列表（按索引进行分组）

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
opts = { 
  index: 56 # Integer | 索引编号
}

begin
  #获取应用下所有索引下的查询列表（按索引进行分组）
  result = api_instance.query_list(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | [optional] 

### Return type

[**Array&lt;QueryBean&gt;**](QueryBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_profile**
> Object query_profile(app, query, opts)

获取搜索诊断信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 记录编号
opts = { 
  q: 'q_example' # String | 诊断关键字
}

begin
  #获取搜索诊断信息
  result = api_instance.query_profile(app, query, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_profile: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 记录编号 | 
 **q** | **String**| 诊断关键字 | [optional] 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_records_of_keyword_binding**
> Array&lt;Object&gt; query_records_of_keyword_binding(app, id)

获取关键词绑定对应的记录列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
id = 56 # Integer | 关键词绑定编号


begin
  #获取关键词绑定对应的记录列表
  result = api_instance.query_records_of_keyword_binding(app, id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_records_of_keyword_binding: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **id** | **Integer**| 关键词绑定编号 | 

### Return type

**Array&lt;Object&gt;**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_save_intelligent_mappings**
> BOOLEAN query_save_intelligent_mappings(app, query, opts)

设置索引智能匹配字段

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号
opts = { 
  fields: ['fields_example'] # Array<String> | 字段列表
}

begin
  #设置索引智能匹配字段
  result = api_instance.query_save_intelligent_mappings(app, query, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_save_intelligent_mappings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 
 **fields** | [**Array&lt;String&gt;**](String.md)| 字段列表 | [optional] 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_save_node_positions**
> Object query_save_node_positions(bodyqueryapp)

保存查询条件的节点位置信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = nil # Object | 
query = 56 # Integer | 查询编号
app = 'app_example' # String | 应用标识


begin
  #保存查询条件的节点位置信息
  result = api_instance.query_save_node_positions(bodyqueryapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_save_node_positions: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Object**](Object.md)|  | 
 **query** | **Integer**| 查询编号 | 
 **app** | **String**| 应用标识 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_search**
> Object query_search(app, query, from, size, opts)

搜索测试

该接口主要用于定制查询的测试，必须授权才能访问

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号
from = 0 # Integer | 起始记录
size = 10 # Integer | 每页记录数量
opts = { 
  q: 'q_example', # String | 查询关键字
  params: {'key' => 'params_example'} # Hash<String, String> | 聚合参数
}

begin
  #搜索测试
  result = api_instance.query_search(app, query, from, size, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_search: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 
 **from** | **Integer**| 起始记录 | [default to 0]
 **size** | **Integer**| 每页记录数量 | [default to 10]
 **q** | **String**| 查询关键字 | [optional] 
 **params** | [**Hash&lt;String, String&gt;**](String.md)| 聚合参数 | [optional] 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_source**
> Object query_source(app, query, opts)

获取最终查询的源码(JSON)

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号
opts = { 
  q: 'q_example' # String | 搜索关键字
}

begin
  #获取最终查询的源码(JSON)
  result = api_instance.query_source(app, query, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_source: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 
 **q** | **String**| 搜索关键字 | [optional] 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_suggest**
> Array&lt;Object&gt; query_suggest(app, query, q)

获取搜索建议列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号
q = 'q_example' # String | 搜索关键字


begin
  #获取搜索建议列表
  result = api_instance.query_suggest(app, query, q)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_suggest: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 
 **q** | **String**| 搜索关键字 | 

### Return type

**Array&lt;Object&gt;**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_test_intelligent_mappings**
> Array&lt;IntelligentMapping&gt; query_test_intelligent_mappings(app, query, q, opts)

测试索引智能匹配字段

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号
q = 'q_example' # String | 搜索内容
opts = { 
  fields: ['fields_example'] # Array<String> | 字段列表
}

begin
  #测试索引智能匹配字段
  result = api_instance.query_test_intelligent_mappings(app, query, q, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_test_intelligent_mappings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 
 **q** | **String**| 搜索内容 | 
 **fields** | [**Array&lt;String&gt;**](String.md)| 字段列表 | [optional] 

### Return type

[**Array&lt;IntelligentMapping&gt;**](IntelligentMapping.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_update**
> QueryBean query_update(bodyqueryapp)

修改查询

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = IndexeaClient::QueryForm.new # QueryForm | 
query = 56 # Integer | 查询编号
app = 'app_example' # String | 应用标识


begin
  #修改查询
  result = api_instance.query_update(bodyqueryapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**QueryForm**](QueryForm.md)|  | 
 **query** | **Integer**| 查询编号 | 
 **app** | **String**| 应用标识 | 

### Return type

[**QueryBean**](QueryBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_update_keyword_bindings**
> KeywordBindingBean query_update_keyword_bindings(bodyqueryapp)

修改关键词文档绑定

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = IndexeaClient::KeywordBindingBean.new # KeywordBindingBean | 
query = 56 # Integer | 查询编号
app = 'app_example' # String | 应用标识


begin
  #修改关键词文档绑定
  result = api_instance.query_update_keyword_bindings(bodyqueryapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_update_keyword_bindings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**KeywordBindingBean**](KeywordBindingBean.md)|  | 
 **query** | **Integer**| 查询编号 | 
 **app** | **String**| 应用标识 | 

### Return type

[**KeywordBindingBean**](KeywordBindingBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_update_settings**
> BOOLEAN query_update_settings(app, query, name, value, type)

更改查询的设置项

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 记录编号
name = 'name_example' # String | 设置项名称
value = 'value_example' # String | 设置值
type = 'string' # String | 设置项类型


begin
  #更改查询的设置项
  result = api_instance.query_update_settings(app, query, name, value, type)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_update_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 记录编号 | 
 **name** | **String**| 设置项名称 | 
 **value** | **String**| 设置值 | 
 **type** | **String**| 设置项类型 | [default to string]

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_update_variable**
> QueryVariableBean query_update_variable(bodyidapp)

修改预定义查询变量

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = IndexeaClient::QueryVariableBean.new # QueryVariableBean | 
id = 56 # Integer | 自定义查询变量编号
app = 'app_example' # String | 应用标识


begin
  #修改预定义查询变量
  result = api_instance.query_update_variable(bodyidapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_update_variable: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**QueryVariableBean**](QueryVariableBean.md)|  | 
 **id** | **Integer**| 自定义查询变量编号 | 
 **app** | **String**| 应用标识 | 

### Return type

[**QueryVariableBean**](QueryVariableBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_validate**
> Object query_validate(app, query)

获取搜索验证结果

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识
query = 56 # Integer | 查询编号


begin
  #获取搜索验证结果
  result = api_instance.query_validate(app, query)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_validate: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **query_validate_aggregation**
> Object query_validate_aggregation(bodyqueryapp)

验证聚合定义是否正确

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = nil # Object | 
query = 56 # Integer | 查询编号
app = 'app_example' # String | 应用标识


begin
  #验证聚合定义是否正确
  result = api_instance.query_validate_aggregation(bodyqueryapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_validate_aggregation: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Object**](Object.md)|  | 
 **query** | **Integer**| 查询编号 | 
 **app** | **String**| 应用标识 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_validate_query**
> Object query_validate_query(bodyqueryapp)

验证聚合定义是否正确

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = nil # Object | 
query = 56 # Integer | 查询编号
app = 'app_example' # String | 应用标识


begin
  #验证聚合定义是否正确
  result = api_instance.query_validate_query(bodyqueryapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_validate_query: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Object**](Object.md)|  | 
 **query** | **Integer**| 查询编号 | 
 **app** | **String**| 应用标识 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_validate_script_field**
> Object query_validate_script_field(bodyapp, opts)

验证脚本字段是否正确

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = nil # Object | 
app = 'app_example' # String | 应用标识
opts = { 
  query: 56 # Integer | 查询编号, query 和 index 两个参数传一个即可
  index: 56 # Integer | 索引编号, query 和 index 两个参数传一个即可
}

begin
  #验证脚本字段是否正确
  result = api_instance.query_validate_script_field(bodyapp, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_validate_script_field: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Object**](Object.md)|  | 
 **app** | **String**| 应用标识 | 
 **query** | **Integer**| 查询编号, query 和 index 两个参数传一个即可 | [optional] 
 **index** | **Integer**| 索引编号, query 和 index 两个参数传一个即可 | [optional] 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_validate_script_score**
> Object query_validate_script_score(bodyqueryapp)

验证脚本字段是否正确

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = nil # Object | 
query = 56 # Integer | 查询编号
app = 'app_example' # String | 应用标识


begin
  #验证脚本字段是否正确
  result = api_instance.query_validate_script_score(bodyqueryapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_validate_script_score: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Object**](Object.md)|  | 
 **query** | **Integer**| 查询编号 | 
 **app** | **String**| 应用标识 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_validate_suggestion**
> Object query_validate_suggestion(bodyqueryapp)

验证建议是否正确

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
body = nil # Object | 
query = 56 # Integer | 查询编号
app = 'app_example' # String | 应用标识


begin
  #验证建议是否正确
  result = api_instance.query_validate_suggestion(bodyqueryapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_validate_suggestion: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Object**](Object.md)|  | 
 **query** | **Integer**| 查询编号 | 
 **app** | **String**| 应用标识 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **query_variables**
> Array&lt;QueryVariableBean&gt; query_variables(app)

获取应用的预定义查询变量列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::QueriesApi.new
app = 'app_example' # String | 应用标识


begin
  #获取应用的预定义查询变量列表
  result = api_instance.query_variables(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling QueriesApi->query_variables: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**Array&lt;QueryVariableBean&gt;**](QueryVariableBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



