# IndexeaClient::IndexTask

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**index** | **Integer** |  | [optional] 
**es_task_id** | **String** |  | [optional] 
**es_detail** | **Object** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**end_at** | **DateTime** |  | [optional] 
**status** | **Integer** |  | [optional] 

