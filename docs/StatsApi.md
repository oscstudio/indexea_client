# IndexeaClient::StatsApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stats_recomms**](StatsApi.md#stats_recomms) | **GET** /stats/{app}/recomms | 获取推荐日志的汇总信息
[**stats_searchs**](StatsApi.md#stats_searchs) | **GET** /stats/{app}/searchs | 获取搜索日志的汇总信息
[**stats_top_clicks**](StatsApi.md#stats_top_clicks) | **GET** /stats/{app}/top-clicks | 获取点击排行榜
[**stats_widgets**](StatsApi.md#stats_widgets) | **GET** /stats/{app}/widgets | 获取模板与组件的统计信息

# **stats_recomms**
> Object stats_recomms(app, opts)

获取推荐日志的汇总信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::StatsApi.new
app = 'app_example' # String | 应用标识
opts = { 
  recomm: 0, # Integer | 统计指定推荐
  start_date: Date.parse('2013-10-20'), # Date | 统计起始日期
  end_date: Date.parse('2013-10-20'), # Date | 统计结束日期
  interval: 'date' # String | 统计间隔 - 日、周、月、季度、年
}

begin
  #获取推荐日志的汇总信息
  result = api_instance.stats_recomms(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling StatsApi->stats_recomms: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **recomm** | **Integer**| 统计指定推荐 | [optional] [default to 0]
 **start_date** | **Date**| 统计起始日期 | [optional] 
 **end_date** | **Date**| 统计结束日期 | [optional] 
 **interval** | **String**| 统计间隔 - 日、周、月、季度、年 | [optional] [default to date]

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **stats_searchs**
> Object stats_searchs(app, opts)

获取搜索日志的汇总信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::StatsApi.new
app = 'app_example' # String | 应用标识
opts = { 
  index: 0, # Integer | 统计指定索引
  query: 0, # Integer | 统计指定查询
  widget: 0, # Integer | 统计指定组件
  start_date: Date.parse('2013-10-20'), # Date | 统计起始日期
  end_date: Date.parse('2013-10-20'), # Date | 统计结束日期
  interval: 'date' # String | 统计间隔 - 日、周、月、季度、年
}

begin
  #获取搜索日志的汇总信息
  result = api_instance.stats_searchs(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling StatsApi->stats_searchs: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 统计指定索引 | [optional] [default to 0]
 **query** | **Integer**| 统计指定查询 | [optional] [default to 0]
 **widget** | **Integer**| 统计指定组件 | [optional] [default to 0]
 **start_date** | **Date**| 统计起始日期 | [optional] 
 **end_date** | **Date**| 统计结束日期 | [optional] 
 **interval** | **String**| 统计间隔 - 日、周、月、季度、年 | [optional] [default to date]

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **stats_top_clicks**
> Array&lt;Object&gt; stats_top_clicks(app, size, opts)

获取点击排行榜

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::StatsApi.new
app = 'app_example' # String | 应用标识
size = 10 # Integer | 排行榜大小
opts = { 
  index: 0, # Integer | 统计指定索引
  query: 0, # Integer | 统计指定查询
  recomm: 0, # Integer | 统计指定推荐组件
  widget: 0, # Integer | 统计指定组件
  start_date: Date.parse('2013-10-20'), # Date | 统计起始日期
  end_date: Date.parse('2013-10-20') # Date | 统计结束日期
}

begin
  #获取点击排行榜
  result = api_instance.stats_top_clicks(app, size, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling StatsApi->stats_top_clicks: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **size** | **Integer**| 排行榜大小 | [default to 10]
 **index** | **Integer**| 统计指定索引 | [optional] [default to 0]
 **query** | **Integer**| 统计指定查询 | [optional] [default to 0]
 **recomm** | **Integer**| 统计指定推荐组件 | [optional] [default to 0]
 **widget** | **Integer**| 统计指定组件 | [optional] [default to 0]
 **start_date** | **Date**| 统计起始日期 | [optional] 
 **end_date** | **Date**| 统计结束日期 | [optional] 

### Return type

**Array&lt;Object&gt;**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **stats_widgets**
> Object stats_widgets(app, opts)

获取模板与组件的统计信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::StatsApi.new
app = 'app_example' # String | 应用标识
opts = { 
  widget: 0, # Integer | 统计指定模板或组件
  start_date: Date.parse('2013-10-20'), # Date | 统计起始日期
  end_date: Date.parse('2013-10-20'), # Date | 统计结束日期
  interval: 'date' # String | 统计间隔 - 日、周、月、季度、年
}

begin
  #获取模板与组件的统计信息
  result = api_instance.stats_widgets(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling StatsApi->stats_widgets: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **widget** | **Integer**| 统计指定模板或组件 | [optional] [default to 0]
 **start_date** | **Date**| 统计起始日期 | [optional] 
 **end_date** | **Date**| 统计结束日期 | [optional] 
 **interval** | **String**| 统计间隔 - 日、周、月、季度、年 | [optional] [default to date]

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



