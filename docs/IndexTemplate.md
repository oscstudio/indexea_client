# IndexeaClient::IndexTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**account** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**intro** | **String** |  | [optional] 
**mappings** | **Object** |  | [optional] 
**query** | **Object** |  | [optional] 
**widgets** | **Array&lt;Object&gt;** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**updated_at** | **DateTime** |  | [optional] 
**opened** | **BOOLEAN** |  | [optional] 
**status** | **Integer** |  | [optional] 

