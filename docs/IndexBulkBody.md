# IndexeaClient::IndexBulkBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**files** | **Array&lt;String&gt;** |  | [optional] 

