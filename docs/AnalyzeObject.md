# IndexeaClient::AnalyzeObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analyzer** | **String** |  | [optional] 
**text** | **String** |  | [optional] 

