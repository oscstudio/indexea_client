# IndexeaClient::RecordFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | **Array&lt;Object&gt;** | 过滤条件 | [optional] 
**aggs** | **Object** | 聚合字段 | [optional] 
**sorts** | **Array&lt;Object&gt;** | 排序字段 | [optional] 
**post_filters** | **Object** | 聚合过滤条件 | [optional] 

