# IndexeaClient::IndexTemplates

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** | 总数 | [optional] 
**items** | [**Array&lt;IndexTemplate&gt;**](IndexTemplate.md) |  | [optional] 

