# IndexeaClient::MessageApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**message_delete**](MessageApi.md#message_delete) | **DELETE** /accounts/message | 删除消息
[**message_feedback**](MessageApi.md#message_feedback) | **POST** /accounts/feedback | 反馈意见
[**message_list**](MessageApi.md#message_list) | **GET** /accounts/message | 获取我相关的消息信息，包括未读消息数量、最新消息等
[**message_read**](MessageApi.md#message_read) | **PATCH** /accounts/message | 标识消息为已读
[**message_send**](MessageApi.md#message_send) | **POST** /accounts/message | 发送消息

# **message_delete**
> BOOLEAN message_delete(id)

删除消息

删除消息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::MessageApi.new
id = 56 # Integer | 消息编号


begin
  #删除消息
  result = api_instance.message_delete(id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling MessageApi->message_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 消息编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **message_feedback**
> BOOLEAN message_feedback(contenttype)

反馈意见

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::MessageApi.new
content = 'content_example' # String | 
type = 'type_example' # String | 


begin
  #反馈意见
  result = api_instance.message_feedback(contenttype)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling MessageApi->message_feedback: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **content** | **String**|  | 
 **type** | **String**|  | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **message_list**
> Messages message_list(scope, size, opts)

获取我相关的消息信息，包括未读消息数量、最新消息等

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::MessageApi.new
scope = 'unread' # String | 
size = 10 # Integer | 消息数量
opts = { 
  from: 0 # Integer | 用于翻页的起始位置
}

begin
  #获取我相关的消息信息，包括未读消息数量、最新消息等
  result = api_instance.message_list(scope, size, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling MessageApi->message_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scope** | **String**|  | [default to unread]
 **size** | **Integer**| 消息数量 | [default to 10]
 **from** | **Integer**| 用于翻页的起始位置 | [optional] [default to 0]

### Return type

[**Messages**](Messages.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **message_read**
> Messages message_read(id)

标识消息为已读

标识消息为已读

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::MessageApi.new
id = 'id_example' # String | 消息编号,多个消息使用逗号隔开


begin
  #标识消息为已读
  result = api_instance.message_read(id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling MessageApi->message_read: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 消息编号,多个消息使用逗号隔开 | 

### Return type

[**Messages**](Messages.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **message_send**
> Message message_send(receivermsg)

发送消息

发送站内消息给某人

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::MessageApi.new
receiver = 56 # Integer | 
msg = 'msg_example' # String | 


begin
  #发送消息
  result = api_instance.message_send(receivermsg)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling MessageApi->message_send: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receiver** | **Integer**|  | 
 **msg** | **String**|  | 

### Return type

[**Message**](Message.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



