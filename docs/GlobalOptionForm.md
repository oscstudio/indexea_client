# IndexeaClient::GlobalOptionForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | 配置项名称 | [optional] 
**value** | **Object** | 配置值 | [optional] 

