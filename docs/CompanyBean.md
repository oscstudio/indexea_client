# IndexeaClient::CompanyBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | 公司名称 | [optional] 
**url** | **String** | 公司网址 | [optional] 
**nation** | **String** | 国家 | [optional] 
**province** | **String** | 省/州/地区 | [optional] 
**city** | **String** | 城市 | [optional] 
**taxpayer** | **String** | 纳税人识别号 | [optional] 
**bank** | **String** | 开户行 | [optional] 
**account** | **String** | 银行账号 | [optional] 
**address** | **String** | 公司注册地址 | [optional] 
**tel** | **String** | 公司电话 | [optional] 
**license** | **String** | 营业执照图片地址 | [optional] 
**certificate** | **String** | 一般纳税人证明扫描件图片地址 | [optional] 
**post_addr** | **String** | 快递地址 | [optional] 
**post_code** | **String** | 邮编 | [optional] 
**post_name** | **String** | 收件人姓名 | [optional] 
**post_tel** | **String** | 收件人电话 | [optional] 
**created_at** | **DateTime** | 首次填写时间 | [optional] 
**updated_at** | **DateTime** | 最后更新时间 | [optional] 

