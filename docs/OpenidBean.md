# IndexeaClient::OpenidBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** | 类型 | [optional] 
**openid** | **String** | 三方账号唯一标识 | [optional] 
**name** | **String** | 三方账号昵称 | [optional] 
**created_at** | **DateTime** | 首次登录时间 | [optional] 

