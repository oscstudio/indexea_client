# IndexeaClient::AnalyzeToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** |  | [optional] 
**start_offset** | [**BigDecimal**](BigDecimal.md) |  | [optional] 
**end_offset** | [**BigDecimal**](BigDecimal.md) |  | [optional] 
**type** | **String** |  | [optional] 
**position** | [**BigDecimal**](BigDecimal.md) |  | [optional] 

