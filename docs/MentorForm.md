# IndexeaClient::MentorForm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** | 成员总数 | [optional] 
**mentors** | [**Array&lt;AccountBean&gt;**](AccountBean.md) | 成员列表 | [optional] 

