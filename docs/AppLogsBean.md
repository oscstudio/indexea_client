# IndexeaClient::AppLogsBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** | 日志总数 | [optional] 
**logs** | [**Array&lt;AppLogBean&gt;**](AppLogBean.md) |  | [optional] 
**excel_url** | **String** | 用于导出 Excel 的地址 | [optional] 

