# IndexeaClient::QueryActionBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**index** | **Integer** |  | [optional] 
**query** | **Integer** |  | [optional] 
**widget** | **Integer** |  | [optional] 
**recomm** | **Integer** |  | [optional] 
**error** | **Object** |  | [optional] 
**user** | **String** |  | [optional] 
**q** | **Array&lt;String&gt;** |  | [optional] 
**referer** | **String** |  | [optional] 
**params** | **Object** |  | [optional] 
**hits** | **Integer** |  | [optional] 
**took** | **Integer** |  | [optional] 
**ip** | **String** |  | [optional] 
**nation** | **String** |  | [optional] 
**province** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**browser** | **String** |  | [optional] 
**os** | **String** |  | [optional] 
**clicks** | **Array&lt;String&gt;** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 

