# IndexeaClient::SearchEstimateResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_purchase_searchs** | **Integer** |  | [optional] 
**last_purchase_searchs** | **Integer** |  | [optional] 
**avail_searchs** | **Integer** |  | [optional] 
**average_searchs** | **Integer** |  | [optional] 
**total_searchs** | **Integer** |  | [optional] 

