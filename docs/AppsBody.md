# IndexeaClient::AppsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**intro** | **String** |  | [optional] 

