# IndexeaClient::IndicesApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**index_cleanup**](IndicesApi.md#index_cleanup) | **POST** /indices/{app}/{index}/cleanup | 清空索引记录
[**index_copy_to**](IndicesApi.md#index_copy_to) | **POST** /indices/{app}/{index}/copyto | 导出索引数据
[**index_create**](IndicesApi.md#index_create) | **POST** /indices/{app} | 创建索引
[**index_create_template**](IndicesApi.md#index_create_template) | **POST** /indices/templates | 创建索引模板
[**index_delete**](IndicesApi.md#index_delete) | **DELETE** /indices/{app}/{index} | 删除索引
[**index_delete_crawler_task**](IndicesApi.md#index_delete_crawler_task) | **DELETE** /indices/{app}/{index}/crawler-settings | 删除索引的数据爬取任务
[**index_delete_template**](IndicesApi.md#index_delete_template) | **DELETE** /indices/templates | 删除索引模板
[**index_export**](IndicesApi.md#index_export) | **POST** /indices/{app}/{index}/export | 导出索引数据
[**index_flush**](IndicesApi.md#index_flush) | **POST** /indices/{app}/{index}/flush | 刷新索引数据，主要用于将内存中的索引数据写入磁盘
[**index_flush_settings**](IndicesApi.md#index_flush_settings) | **PUT** /indices/{app}/{index}/settings | 写入设置信息到索引
[**index_get**](IndicesApi.md#index_get) | **GET** /indices/{app}/{index} | 获取单个索引详情
[**index_get_crawler_logs**](IndicesApi.md#index_get_crawler_logs) | **GET** /indices/{app}/{index}/crawler-logs | 获取索引的爬虫任务的爬取日志
[**index_get_crawler_task**](IndicesApi.md#index_get_crawler_task) | **GET** /indices/{app}/{index}/crawler-settings | 获取索引的爬虫任务设定
[**index_get_filter_settings**](IndicesApi.md#index_get_filter_settings) | **GET** /indices/{app}/{index}/filter-settings | 获取索引设置信息
[**index_get_settings**](IndicesApi.md#index_get_settings) | **GET** /indices/{app}/{index}/settings | 获取索引设置信息
[**index_list**](IndicesApi.md#index_list) | **GET** /indices/{app} | 获取应用的索引列表
[**index_list_templates**](IndicesApi.md#index_list_templates) | **GET** /indices/templates | 获取所有可用的索引模板
[**index_prefetch**](IndicesApi.md#index_prefetch) | **GET** /indices/crawler | 获取目标网站内容预览
[**index_rebuild**](IndicesApi.md#index_rebuild) | **POST** /indices/{app}/{index}/rebuild | 重建索引数据
[**index_rebuild_task**](IndicesApi.md#index_rebuild_task) | **GET** /indices/{app}/{index}/rebuild | 获取重建索引任务的详情
[**index_submit_crawler_task**](IndicesApi.md#index_submit_crawler_task) | **POST** /indices/{app}/{index}/crawler-settings | 提交或者更新索引的数据爬取任务
[**index_update**](IndicesApi.md#index_update) | **PUT** /indices/{app}/{index} | 修改索引
[**index_update_settings**](IndicesApi.md#index_update_settings) | **POST** /indices/{app}/{index}/settings | 更新索引设置信息
[**index_update_template**](IndicesApi.md#index_update_template) | **PUT** /indices/templates | 修改索引模板
[**synonyms_add**](IndicesApi.md#synonyms_add) | **POST** /indices/{app}/{index}/synonyms | 添加同义词
[**synonyms_delete**](IndicesApi.md#synonyms_delete) | **DELETE** /indices/{app}/{index}/synonyms | 删除同义词
[**synonyms_enable**](IndicesApi.md#synonyms_enable) | **PATCH** /indices/{app}/{index}/synonyms | 启用禁用同义词
[**synonyms_flush**](IndicesApi.md#synonyms_flush) | **POST** /indices/{app}/{index}/synonyms-flush | 将同义词更新到搜索引擎的同义词表
[**synonyms_import**](IndicesApi.md#synonyms_import) | **POST** /indices/{app}/{index}/synonyms-import | 导入同义词
[**synonyms_list**](IndicesApi.md#synonyms_list) | **GET** /indices/{app}/{index}/synonyms | 获取索引的所有同义词
[**synonyms_update**](IndicesApi.md#synonyms_update) | **PUT** /indices/{app}/{index}/synonyms | 修改同义词

# **index_cleanup**
> Object index_cleanup(app, index, vcode)

清空索引记录

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
vcode = 'vcode_example' # String | 验证码


begin
  #清空索引记录
  result = api_instance.index_cleanup(app, index, vcode)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_cleanup: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **vcode** | **String**| 验证码 | 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_copy_to**
> IndexBean index_copy_to(app, index, name, fields, queries, records)

导出索引数据

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
name = 'name_example' # String | 新索引的名称
fields = true # BOOLEAN | 是否复制字段定义
queries = true # BOOLEAN | 是否复制所有查询
records = true # BOOLEAN | 是否复制所有文档


begin
  #导出索引数据
  result = api_instance.index_copy_to(app, index, name, fields, queries, records)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_copy_to: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **name** | **String**| 新索引的名称 | 
 **fields** | **BOOLEAN**| 是否复制字段定义 | 
 **queries** | **BOOLEAN**| 是否复制所有查询 | 
 **records** | **BOOLEAN**| 是否复制所有文档 | 

### Return type

[**IndexBean**](IndexBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_create**
> IndexBean index_create(bodyapp)

创建索引

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
body = IndexeaClient::IndexForm.new # IndexForm | 
app = 'app_example' # String | 应用标识


begin
  #创建索引
  result = api_instance.index_create(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IndexForm**](IndexForm.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

[**IndexBean**](IndexBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **index_create_template**
> IndexTemplate index_create_template(body)

创建索引模板

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
body = IndexeaClient::IndexTemplate.new # IndexTemplate | 


begin
  #创建索引模板
  result = api_instance.index_create_template(body)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_create_template: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IndexTemplate**](IndexTemplate.md)|  | 

### Return type

[**IndexTemplate**](IndexTemplate.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **index_delete**
> BOOLEAN index_delete(appindex, opts)

删除索引

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
opts = { 
  vcode: 'vcode_example' # String | 
}

begin
  #删除索引
  result = api_instance.index_delete(appindex, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **vcode** | **String**|  | [optional] 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **index_delete_crawler_task**
> BOOLEAN index_delete_crawler_task(app, index)

删除索引的数据爬取任务

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #删除索引的数据爬取任务
  result = api_instance.index_delete_crawler_task(app, index)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_delete_crawler_task: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_delete_template**
> BOOLEAN index_delete_template(id)

删除索引模板

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
id = 56 # Integer | 


begin
  #删除索引模板
  result = api_instance.index_delete_template(id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_delete_template: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_export**
> BOOLEAN index_export(app, index, format)

导出索引数据

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
format = 'json' # String | 导出数据的格式


begin
  #导出索引数据
  result = api_instance.index_export(app, index, format)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_export: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **format** | **String**| 导出数据的格式 | [default to json]

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_flush**
> BOOLEAN index_flush(app, index)

刷新索引数据，主要用于将内存中的索引数据写入磁盘

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #刷新索引数据，主要用于将内存中的索引数据写入磁盘
  result = api_instance.index_flush(app, index)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_flush: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_flush_settings**
> Object index_flush_settings(typeappindex, opts)

写入设置信息到索引

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
type = 'type_example' # String | 设置类型
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
opts = { 
  password: 'password_example' # String | 
}

begin
  #写入设置信息到索引
  result = api_instance.index_flush_settings(typeappindex, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_flush_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**| 设置类型 | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **password** | [**String**](.md)|  | [optional] 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **index_get**
> IndexBean index_get(app, index)

获取单个索引详情

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #获取单个索引详情
  result = api_instance.index_get(app, index)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

[**IndexBean**](IndexBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_get_crawler_logs**
> CrawlerLogs index_get_crawler_logs(app, index, from, size)

获取索引的爬虫任务的爬取日志

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
from = 56 # Integer | 
size = 56 # Integer | 


begin
  #获取索引的爬虫任务的爬取日志
  result = api_instance.index_get_crawler_logs(app, index, from, size)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_get_crawler_logs: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **from** | **Integer**|  | 
 **size** | **Integer**|  | 

### Return type

[**CrawlerLogs**](CrawlerLogs.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_get_crawler_task**
> CrawlerTask index_get_crawler_task(app, index)

获取索引的爬虫任务设定

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #获取索引的爬虫任务设定
  result = api_instance.index_get_crawler_task(app, index)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_get_crawler_task: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

[**CrawlerTask**](CrawlerTask.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_get_filter_settings**
> Array&lt;String&gt; index_get_filter_settings(app, index, type)

获取索引设置信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
type = 'type_example' # String | 设置类型


begin
  #获取索引设置信息
  result = api_instance.index_get_filter_settings(app, index, type)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_get_filter_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **type** | **String**| 设置类型 | 

### Return type

**Array&lt;String&gt;**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_get_settings**
> IndexSettings index_get_settings(app, index, type)

获取索引设置信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
type = 'type_example' # String | 设置类型


begin
  #获取索引设置信息
  result = api_instance.index_get_settings(app, index, type)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_get_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **type** | **String**| 设置类型 | 

### Return type

[**IndexSettings**](IndexSettings.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_list**
> Array&lt;IndexBean&gt; index_list(app, opts)

获取应用的索引列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
opts = { 
  stat: true # BOOLEAN | 是否包含索引的统计信息
}

begin
  #获取应用的索引列表
  result = api_instance.index_list(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **stat** | **BOOLEAN**| 是否包含索引的统计信息 | [optional] [default to true]

### Return type

[**Array&lt;IndexBean&gt;**](IndexBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_list_templates**
> IndexTemplates index_list_templates(from, size)

获取所有可用的索引模板

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
from = 0 # Integer | 
size = 100 # Integer | 


begin
  #获取所有可用的索引模板
  result = api_instance.index_list_templates(from, size)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_list_templates: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from** | **Integer**|  | [default to 0]
 **size** | **Integer**|  | [default to 100]

### Return type

[**IndexTemplates**](IndexTemplates.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_prefetch**
> Array&lt;Object&gt; index_prefetch(type, url)

获取目标网站内容预览

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
type = 'type_example' # String | 
url = 'url_example' # String | 


begin
  #获取目标网站内容预览
  result = api_instance.index_prefetch(type, url)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_prefetch: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**|  | 
 **url** | **String**|  | 

### Return type

**Array&lt;Object&gt;**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_rebuild**
> IndexTask index_rebuild(bodyappindex)

重建索引数据

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
body = IndexeaClient::IndexRebuildForm.new # IndexRebuildForm | 
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #重建索引数据
  result = api_instance.index_rebuild(bodyappindex)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_rebuild: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IndexRebuildForm**](IndexRebuildForm.md)|  | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

[**IndexTask**](IndexTask.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **index_rebuild_task**
> IndexTask index_rebuild_task(app, index, task_id)

获取重建索引任务的详情

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
task_id = 0 # Integer | 任务编号,传0则获取最新的任务信息


begin
  #获取重建索引任务的详情
  result = api_instance.index_rebuild_task(app, index, task_id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_rebuild_task: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **task_id** | **Integer**| 任务编号,传0则获取最新的任务信息 | [default to 0]

### Return type

[**IndexTask**](IndexTask.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **index_submit_crawler_task**
> CrawlerTask index_submit_crawler_task(bodyappindex)

提交或者更新索引的数据爬取任务

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
body = IndexeaClient::CrawlerTask.new # CrawlerTask | 
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #提交或者更新索引的数据爬取任务
  result = api_instance.index_submit_crawler_task(bodyappindex)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_submit_crawler_task: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CrawlerTask**](CrawlerTask.md)|  | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

[**CrawlerTask**](CrawlerTask.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **index_update**
> IndexBean index_update(bodyappindex)

修改索引

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
body = IndexeaClient::IndexForm.new # IndexForm | 
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #修改索引
  result = api_instance.index_update(bodyappindex)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IndexForm**](IndexForm.md)|  | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

[**IndexBean**](IndexBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **index_update_settings**
> BOOLEAN index_update_settings(typeappindex, opts)

更新索引设置信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
type = 'type_example' # String | 设置类型
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
opts = { 
  body: 'body_example' # String | 
}

begin
  #更新索引设置信息
  result = api_instance.index_update_settings(typeappindex, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_update_settings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**| 设置类型 | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **body** | [**String**](String.md)|  | [optional] 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: text/plain
 - **Accept**: application/json



# **index_update_template**
> IndexTemplate index_update_template(body)

修改索引模板

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
body = IndexeaClient::IndexTemplate.new # IndexTemplate | 


begin
  #修改索引模板
  result = api_instance.index_update_template(body)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->index_update_template: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**IndexTemplate**](IndexTemplate.md)|  | 

### Return type

[**IndexTemplate**](IndexTemplate.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **synonyms_add**
> SynonymsBean synonyms_add(bodyappindex)

添加同义词

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
body = IndexeaClient::SynonymsBean.new # SynonymsBean | 
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #添加同义词
  result = api_instance.synonyms_add(bodyappindex)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->synonyms_add: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**SynonymsBean**](SynonymsBean.md)|  | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

[**SynonymsBean**](SynonymsBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **synonyms_delete**
> BOOLEAN synonyms_delete(app, index, id)

删除同义词

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
id = 56 # Integer | 同义词编号


begin
  #删除同义词
  result = api_instance.synonyms_delete(app, index, id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->synonyms_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **id** | **Integer**| 同义词编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **synonyms_enable**
> BOOLEAN synonyms_enable(app, index, id, opts)

启用禁用同义词

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
id = 56 # Integer | 同义词编号
opts = { 
  enable: true, # BOOLEAN | 是否启用
  all: true # BOOLEAN | 是否对所有索引起作用
}

begin
  #启用禁用同义词
  result = api_instance.synonyms_enable(app, index, id, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->synonyms_enable: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **id** | **Integer**| 同义词编号 | 
 **enable** | **BOOLEAN**| 是否启用 | [optional] 
 **all** | **BOOLEAN**| 是否对所有索引起作用 | [optional] 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **synonyms_flush**
> Object synonyms_flush(appindex, opts)

将同义词更新到搜索引擎的同义词表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
opts = { 
  password: 'password_example' # String | 
}

begin
  #将同义词更新到搜索引擎的同义词表
  result = api_instance.synonyms_flush(appindex, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->synonyms_flush: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **password** | [**String**](.md)|  | [optional] 

### Return type

**Object**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **synonyms_import**
> Array&lt;SynonymsBean&gt; synonyms_import(bodyactionappindex)

导入同义词

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
body = [IndexeaClient::SynonymsBean.new] # Array<SynonymsBean> | 
action = 56 # Integer | 覆盖方式
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #导入同义词
  result = api_instance.synonyms_import(bodyactionappindex)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->synonyms_import: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Array&lt;SynonymsBean&gt;**](SynonymsBean.md)|  | 
 **action** | **Integer**| 覆盖方式 | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

[**Array&lt;SynonymsBean&gt;**](SynonymsBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **synonyms_list**
> Array&lt;SynonymsBean&gt; synonyms_list(app, index, size, opts)

获取索引的所有同义词

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号
size = 99999 # Integer | 
opts = { 
  type: 0, # Integer | 类型
  from: 0 # Integer | 
}

begin
  #获取索引的所有同义词
  result = api_instance.synonyms_list(app, index, size, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->synonyms_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 
 **size** | **Integer**|  | [default to 99999]
 **type** | **Integer**| 类型 | [optional] [default to 0]
 **from** | **Integer**|  | [optional] [default to 0]

### Return type

[**Array&lt;SynonymsBean&gt;**](SynonymsBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **synonyms_update**
> SynonymsBean synonyms_update(bodyappindex)

修改同义词

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::IndicesApi.new
body = IndexeaClient::SynonymsBean.new # SynonymsBean | 
app = 'app_example' # String | 应用标识
index = 56 # Integer | 索引编号


begin
  #修改同义词
  result = api_instance.synonyms_update(bodyappindex)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling IndicesApi->synonyms_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**SynonymsBean**](SynonymsBean.md)|  | 
 **app** | **String**| 应用标识 | 
 **index** | **Integer**| 索引编号 | 

### Return type

[**SynonymsBean**](SynonymsBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



