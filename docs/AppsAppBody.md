# IndexeaClient::AppsAppBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**intro** | **String** |  | [optional] 

