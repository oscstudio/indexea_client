# IndexeaClient::AppsApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**app_add_mentor**](AppsApi.md#app_add_mentor) | **POST** /apps/{app}/mentors | 添加应用成员
[**app_blacklist**](AppsApi.md#app_blacklist) | **GET** /apps/{app}/blacklist | 获取黑名单信息
[**app_create**](AppsApi.md#app_create) | **POST** /apps | 创建应用
[**app_create_access_token**](AppsApi.md#app_create_access_token) | **POST** /apps/{app}/tokens | 创建 Access Token
[**app_create_oauth_app**](AppsApi.md#app_create_oauth_app) | **POST** /apps/{app}/oauth | 创建第三方应用
[**app_delete**](AppsApi.md#app_delete) | **DELETE** /apps/{app} | 删除应用
[**app_delete_access_token**](AppsApi.md#app_delete_access_token) | **DELETE** /apps/{app}/tokens | 删除 Access Token
[**app_delete_mentor**](AppsApi.md#app_delete_mentor) | **DELETE** /apps/{app}/mentors | 删除应用成员
[**app_delete_oauth_app**](AppsApi.md#app_delete_oauth_app) | **DELETE** /apps/{app}/oauth | 删除第三方应用
[**app_excel_of_logs**](AppsApi.md#app_excel_of_logs) | **GET** /apps/{app}/logs | 导出应用日志到 Excel
[**app_get**](AppsApi.md#app_get) | **GET** /apps/{app} | 获取应用详情
[**app_get_company**](AppsApi.md#app_get_company) | **POST** /apps/{app}/company | 获取应用填写的公司信息
[**app_get_company_pic**](AppsApi.md#app_get_company_pic) | **GET** /apps/{app}/company | 获取公司营业执照或者一般纳税人证明
[**app_list**](AppsApi.md#app_list) | **GET** /apps | 获取应用列表
[**app_list_mentors**](AppsApi.md#app_list_mentors) | **GET** /apps/{app}/mentors | 获取应用成员列表
[**app_list_oauth_apps**](AppsApi.md#app_list_oauth_apps) | **GET** /apps/{app}/oauth | 获取第三方应用列表
[**app_logs**](AppsApi.md#app_logs) | **POST** /apps/{app}/logs | 获取应用的日志列表
[**app_reset_access_token**](AppsApi.md#app_reset_access_token) | **PATCH** /apps/{app}/tokens | 重置 Access Token
[**app_reset_oauth_app_secret**](AppsApi.md#app_reset_oauth_app_secret) | **POST** /apps/{app}/oauth-reset-secret | 重新生成三方应用的密钥
[**app_save_blacklist**](AppsApi.md#app_save_blacklist) | **PUT** /apps/{app}/blacklist | 修改应用的黑名单信息
[**app_save_company**](AppsApi.md#app_save_company) | **PUT** /apps/{app}/company | 修改应用的公司信息
[**app_searchs_estimate**](AppsApi.md#app_searchs_estimate) | **GET** /apps/{app}/searchs-estimate | 获取搜索流量包使用配额信息
[**app_set_trigger**](AppsApi.md#app_set_trigger) | **PUT** /apps/{app}/trigger | 修改应用的触发器信息
[**app_tokens**](AppsApi.md#app_tokens) | **GET** /apps/{app}/tokens | 获取 Access Token 列表
[**app_transfer**](AppsApi.md#app_transfer) | **POST** /apps/{app}/transfer | 转让应用给他人
[**app_trigger**](AppsApi.md#app_trigger) | **GET** /apps/{app}/trigger | 获取应用触发器详情
[**app_trigger_logs**](AppsApi.md#app_trigger_logs) | **GET** /apps/{app}/trigger-logs | 获取应用触发日志列表
[**app_update**](AppsApi.md#app_update) | **PUT** /apps/{app} | 修改应用的基本信息
[**app_update_access_token**](AppsApi.md#app_update_access_token) | **PUT** /apps/{app}/tokens | 修改 Access Token
[**app_update_mentor**](AppsApi.md#app_update_mentor) | **PATCH** /apps/{app}/mentors | 修改成员备注和权限
[**app_update_mentor_options**](AppsApi.md#app_update_mentor_options) | **POST** /apps/{app}/mentors-options | 修改应用成员自身的设置（包括应用名备注，是否接收报告等）
[**app_update_mentor_report_options**](AppsApi.md#app_update_mentor_report_options) | **PATCH** /apps/{app}/mentors-options | 修改应用成员自身的通知设置
[**app_update_oauth_app**](AppsApi.md#app_update_oauth_app) | **PATCH** /apps/{app}/oauth | 修改第三方应用信息
[**app_update_oauth_app_logo**](AppsApi.md#app_update_oauth_app_logo) | **PUT** /apps/{app}/oauth | 修改三方应用图标
[**app_update_status**](AppsApi.md#app_update_status) | **PATCH** /apps/{app} | 修改应用的状态

# **app_add_mentor**
> MentorForm app_add_mentor(app, account, scopes, opts)

添加应用成员

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
account = 'account_example' # String | 成员账号
scopes = 'scopes_example' # String | 权限
opts = { 
  name: 'name_example' # String | 备注名称
}

begin
  #添加应用成员
  result = api_instance.app_add_mentor(app, account, scopes, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_add_mentor: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **account** | **String**| 成员账号 | 
 **scopes** | **String**| 权限 | 
 **name** | **String**| 备注名称 | [optional] 

### Return type

[**MentorForm**](MentorForm.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_blacklist**
> BlacklistBean app_blacklist(app)

获取黑名单信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识


begin
  #获取黑名单信息
  result = api_instance.app_blacklist(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_blacklist: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**BlacklistBean**](BlacklistBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_create**
> AppBean app_create(opts)

创建应用

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
opts = { 
  name: 'name_example' # String | 
  intro: 'intro_example' # String | 
}

begin
  #创建应用
  result = api_instance.app_create(opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  | [optional] 
 **intro** | **String**|  | [optional] 

### Return type

[**AppBean**](AppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **app_create_access_token**
> TokenBean app_create_access_token(bodyapp)

创建 Access Token

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
body = IndexeaClient::TokenBean.new # TokenBean | 
app = 'app_example' # String | 应用标识


begin
  #创建 Access Token
  result = api_instance.app_create_access_token(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_create_access_token: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenBean**](TokenBean.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

[**TokenBean**](TokenBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **app_create_oauth_app**
> OauthAppBean app_create_oauth_app(bodyapp)

创建第三方应用

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
body = IndexeaClient::OauthAppBean.new # OauthAppBean | 
app = 'app_example' # String | 应用标识


begin
  #创建第三方应用
  result = api_instance.app_create_oauth_app(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_create_oauth_app: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**OauthAppBean**](OauthAppBean.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

[**OauthAppBean**](OauthAppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **app_delete**
> BOOLEAN app_delete(app)

删除应用

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识


begin
  #删除应用
  result = api_instance.app_delete(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_delete_access_token**
> BOOLEAN app_delete_access_token(idapp, opts)

删除 Access Token

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
id = 56 # Integer | Access Token 编号
app = 'app_example' # String | 应用标识
opts = { 
  vcode: 'vcode_example' # String | 
}

begin
  #删除 Access Token
  result = api_instance.app_delete_access_token(idapp, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_delete_access_token: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Access Token 编号 | 
 **app** | **String**| 应用标识 | 
 **vcode** | **String**|  | [optional] 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **app_delete_mentor**
> BOOLEAN app_delete_mentor(app, account)

删除应用成员

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
account = 56 # Integer | 成员编号


begin
  #删除应用成员
  result = api_instance.app_delete_mentor(app, account)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_delete_mentor: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **account** | **Integer**| 成员编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_delete_oauth_app**
> BOOLEAN app_delete_oauth_app(identapp, opts)

删除第三方应用

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
ident = 'ident_example' # String | 三方应用编号
app = 'app_example' # String | 应用标识
opts = { 
  vcode: 'vcode_example' # String | 
}

begin
  #删除第三方应用
  result = api_instance.app_delete_oauth_app(identapp, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_delete_oauth_app: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **String**| 三方应用编号 | 
 **app** | **String**| 应用标识 | 
 **vcode** | **String**|  | [optional] 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **app_excel_of_logs**
> String app_excel_of_logs(app, opts)

导出应用日志到 Excel

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
opts = { 
  account: 0, # Integer | 账号
  indices: [56], # Array<Integer> | 索引
  widget: 0, # Integer | 组件
  query: 0, # Integer | 查询
  type: 0, # Integer | 类型
  start_date: Date.parse('2013-10-20'), # Date | 起始日期
  end_date: Date.parse('2013-10-20') # Date | 结束日期
}

begin
  #导出应用日志到 Excel
  result = api_instance.app_excel_of_logs(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_excel_of_logs: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **account** | **Integer**| 账号 | [optional] [default to 0]
 **indices** | [**Array&lt;Integer&gt;**](Integer.md)| 索引 | [optional] 
 **widget** | **Integer**| 组件 | [optional] [default to 0]
 **query** | **Integer**| 查询 | [optional] [default to 0]
 **type** | **Integer**| 类型 | [optional] [default to 0]
 **start_date** | **Date**| 起始日期 | [optional] 
 **end_date** | **Date**| 结束日期 | [optional] 

### Return type

**String**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet



# **app_get**
> AppBean app_get(app)

获取应用详情

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识


begin
  #获取应用详情
  result = api_instance.app_get(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**AppBean**](AppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_get_company**
> CompanyBean app_get_company(app)

获取应用填写的公司信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识


begin
  #获取应用填写的公司信息
  result = api_instance.app_get_company(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_get_company: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**CompanyBean**](CompanyBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_get_company_pic**
> String app_get_company_pic(app, type)

获取公司营业执照或者一般纳税人证明

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
type = 'type_example' # String | 获取图片的类型 [license,certificate]


begin
  #获取公司营业执照或者一般纳税人证明
  result = api_instance.app_get_company_pic(app, type)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_get_company_pic: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **type** | **String**| 获取图片的类型 [license,certificate] | 

### Return type

**String**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: default



# **app_list**
> Array&lt;AppBean&gt; app_list

获取应用列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new

begin
  #获取应用列表
  result = api_instance.app_list
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_list: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Array&lt;AppBean&gt;**](AppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_list_mentors**
> MentorForm app_list_mentors(app, opts)

获取应用成员列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
opts = { 
  from: 0, # Integer | 开始位置
  size: 20 # Integer | 获取的数量
}

begin
  #获取应用成员列表
  result = api_instance.app_list_mentors(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_list_mentors: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **from** | **Integer**| 开始位置 | [optional] [default to 0]
 **size** | **Integer**| 获取的数量 | [optional] [default to 20]

### Return type

[**MentorForm**](MentorForm.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_list_oauth_apps**
> Array&lt;OauthAppBean&gt; app_list_oauth_apps(app)

获取第三方应用列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识


begin
  #获取第三方应用列表
  result = api_instance.app_list_oauth_apps(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_list_oauth_apps: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**Array&lt;OauthAppBean&gt;**](OauthAppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_logs**
> AppLogsBean app_logs(app, opts)

获取应用的日志列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
opts = { 
  account: 0, # Integer | 账号
  indices: [56], # Array<Integer> | 索引
  widget: 0, # Integer | 组件
  query: 0, # Integer | 查询
  type: 0, # Integer | 类型
  start_date: Date.parse('2013-10-20'), # Date | 起始日期
  end_date: Date.parse('2013-10-20'), # Date | 结束日期
  from: 0, # Integer | 起始位置
  size: 20 # Integer | 数量
}

begin
  #获取应用的日志列表
  result = api_instance.app_logs(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_logs: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **account** | **Integer**| 账号 | [optional] [default to 0]
 **indices** | [**Array&lt;Integer&gt;**](Integer.md)| 索引 | [optional] 
 **widget** | **Integer**| 组件 | [optional] [default to 0]
 **query** | **Integer**| 查询 | [optional] [default to 0]
 **type** | **Integer**| 类型 | [optional] [default to 0]
 **start_date** | **Date**| 起始日期 | [optional] 
 **end_date** | **Date**| 结束日期 | [optional] 
 **from** | **Integer**| 起始位置 | [optional] [default to 0]
 **size** | **Integer**| 数量 | [optional] [default to 20]

### Return type

[**AppLogsBean**](AppLogsBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_reset_access_token**
> TokenBean app_reset_access_token(idapp, opts)

重置 Access Token

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
id = 56 # Integer | Access Token 编号
app = 'app_example' # String | 应用标识
opts = { 
  vcode: 'vcode_example' # String | 
}

begin
  #重置 Access Token
  result = api_instance.app_reset_access_token(idapp, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_reset_access_token: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Access Token 编号 | 
 **app** | **String**| 应用标识 | 
 **vcode** | **String**|  | [optional] 

### Return type

[**TokenBean**](TokenBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **app_reset_oauth_app_secret**
> OauthAppBean app_reset_oauth_app_secret(identapp, opts)

重新生成三方应用的密钥

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
ident = 'ident_example' # String | 三方应用标识
app = 'app_example' # String | 应用标识
opts = { 
  vcode: 'vcode_example' # String | 
}

begin
  #重新生成三方应用的密钥
  result = api_instance.app_reset_oauth_app_secret(identapp, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_reset_oauth_app_secret: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **String**| 三方应用标识 | 
 **app** | **String**| 应用标识 | 
 **vcode** | **String**|  | [optional] 

### Return type

[**OauthAppBean**](OauthAppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **app_save_blacklist**
> BOOLEAN app_save_blacklist(bodyapp)

修改应用的黑名单信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
body = IndexeaClient::BlacklistBean.new # BlacklistBean | 
app = 'app_example' # String | 应用标识


begin
  #修改应用的黑名单信息
  result = api_instance.app_save_blacklist(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_save_blacklist: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BlacklistBean**](BlacklistBean.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **app_save_company**
> CompanyBean app_save_company(app, opts)

修改应用的公司信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
opts = { 
  name: 'name_example' # String | 
  url: 'url_example' # String | 
  nation: 'nation_example' # String | 
  province: 'province_example' # String | 
  city: 'city_example' # String | 
  taxpayer: 'taxpayer_example' # String | 
  bank: 'bank_example' # String | 
  account: 'account_example' # String | 
  address: 'address_example' # String | 
  tel: 'tel_example' # String | 
  license: 'license_example' # String | 
  certificate: 'certificate_example' # String | 
  post_addr: 'post_addr_example' # String | 
  post_code: 'post_code_example' # String | 
  post_name: 'post_name_example' # String | 
  post_tel: 'post_tel_example' # String | 
}

begin
  #修改应用的公司信息
  result = api_instance.app_save_company(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_save_company: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **name** | **String**|  | [optional] 
 **url** | **String**|  | [optional] 
 **nation** | **String**|  | [optional] 
 **province** | **String**|  | [optional] 
 **city** | **String**|  | [optional] 
 **taxpayer** | **String**|  | [optional] 
 **bank** | **String**|  | [optional] 
 **account** | **String**|  | [optional] 
 **address** | **String**|  | [optional] 
 **tel** | **String**|  | [optional] 
 **license** | **String**|  | [optional] 
 **certificate** | **String**|  | [optional] 
 **post_addr** | **String**|  | [optional] 
 **post_code** | **String**|  | [optional] 
 **post_name** | **String**|  | [optional] 
 **post_tel** | **String**|  | [optional] 

### Return type

[**CompanyBean**](CompanyBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json



# **app_searchs_estimate**
> SearchEstimateResult app_searchs_estimate(app, days)

获取搜索流量包使用配额信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
days = 30 # Integer | 计算平均搜索数的最近天数


begin
  #获取搜索流量包使用配额信息
  result = api_instance.app_searchs_estimate(app, days)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_searchs_estimate: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **days** | **Integer**| 计算平均搜索数的最近天数 | [default to 30]

### Return type

[**SearchEstimateResult**](SearchEstimateResult.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_set_trigger**
> BOOLEAN app_set_trigger(bodyapp)

修改应用的触发器信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
body = IndexeaClient::TriggerBean.new # TriggerBean | 
app = 'app_example' # String | 应用标识


begin
  #修改应用的触发器信息
  result = api_instance.app_set_trigger(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_set_trigger: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TriggerBean**](TriggerBean.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **app_tokens**
> Array&lt;TokenBean&gt; app_tokens(app)

获取 Access Token 列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识


begin
  #获取 Access Token 列表
  result = api_instance.app_tokens(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_tokens: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**Array&lt;TokenBean&gt;**](TokenBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_transfer**
> BOOLEAN app_transfer(app, vcode, account)

转让应用给他人

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
vcode = 'vcode_example' # String | 验证码
account = 56 # Integer | 目标账号


begin
  #转让应用给他人
  result = api_instance.app_transfer(app, vcode, account)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_transfer: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **vcode** | **String**| 验证码 | 
 **account** | **Integer**| 目标账号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_trigger**
> TriggerBean app_trigger(app)

获取应用触发器详情

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识


begin
  #获取应用触发器详情
  result = api_instance.app_trigger(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_trigger: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**TriggerBean**](TriggerBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_trigger_logs**
> Array&lt;TriggerLogBean&gt; app_trigger_logs(app, id, size)

获取应用触发日志列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
id = 999999999 # Integer | 起始日志编号
size = 20 # Integer | 日志数


begin
  #获取应用触发日志列表
  result = api_instance.app_trigger_logs(app, id, size)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_trigger_logs: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **id** | **Integer**| 起始日志编号 | [default to 999999999]
 **size** | **Integer**| 日志数 | [default to 20]

### Return type

[**Array&lt;TriggerLogBean&gt;**](TriggerLogBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_update**
> AppBean app_update(app, opts)

修改应用的基本信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
opts = { 
  name: 'name_example' # String | 
  intro: 'intro_example' # String | 
}

begin
  #修改应用的基本信息
  result = api_instance.app_update(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **name** | **String**|  | [optional] 
 **intro** | **String**|  | [optional] 

### Return type

[**AppBean**](AppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **app_update_access_token**
> TokenBean app_update_access_token(bodyapp)

修改 Access Token

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
body = IndexeaClient::TokenBean.new # TokenBean | 
app = 'app_example' # String | 应用标识


begin
  #修改 Access Token
  result = api_instance.app_update_access_token(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_update_access_token: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenBean**](TokenBean.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

[**TokenBean**](TokenBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **app_update_mentor**
> BOOLEAN app_update_mentor(app, account, name, scopes)

修改成员备注和权限

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
account = 56 # Integer | 成员编号
name = 'name_example' # String | 备注名称
scopes = 'scopes_example' # String | 权限


begin
  #修改成员备注和权限
  result = api_instance.app_update_mentor(app, account, name, scopes)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_update_mentor: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **account** | **Integer**| 成员编号 | 
 **name** | **String**| 备注名称 | 
 **scopes** | **String**| 权限 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_update_mentor_options**
> BOOLEAN app_update_mentor_options(app, opts)

修改应用成员自身的设置（包括应用名备注，是否接收报告等）

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
opts = { 
  name: 'name_example', # String | 应用备注名称，如果不填写则使用默认名称
  report: false # BOOLEAN | 是否接收使用情况报告
}

begin
  #修改应用成员自身的设置（包括应用名备注，是否接收报告等）
  result = api_instance.app_update_mentor_options(app, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_update_mentor_options: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **name** | **String**| 应用备注名称，如果不填写则使用默认名称 | [optional] 
 **report** | **BOOLEAN**| 是否接收使用情况报告 | [optional] [default to false]

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **app_update_mentor_report_options**
> BOOLEAN app_update_mentor_report_options(keytypevaluevcodeapp)

修改应用成员自身的通知设置

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
key = 'key_example' # String | 
type = 'type_example' # String | 
value = 'value_example' # String | 
vcode = 'vcode_example' # String | 
app = 'app_example' # String | 应用标识


begin
  #修改应用成员自身的通知设置
  result = api_instance.app_update_mentor_report_options(keytypevaluevcodeapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_update_mentor_report_options: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**|  | 
 **type** | **String**|  | 
 **value** | **String**|  | 
 **vcode** | **String**|  | 
 **app** | **String**| 应用标识 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json



# **app_update_oauth_app**
> OauthAppBean app_update_oauth_app(bodyapp)

修改第三方应用信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
body = IndexeaClient::OauthAppBean.new # OauthAppBean | 
app = 'app_example' # String | 应用标识


begin
  #修改第三方应用信息
  result = api_instance.app_update_oauth_app(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_update_oauth_app: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**OauthAppBean**](OauthAppBean.md)|  | 
 **app** | **String**| 应用标识 | 

### Return type

[**OauthAppBean**](OauthAppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **app_update_oauth_app_logo**
> OauthAppBean app_update_oauth_app_logo(identapp, opts)

修改三方应用图标

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
ident = 'ident_example' # String | 三方应用标识
app = 'app_example' # String | 应用标识
opts = { 
  logo: 'logo_example' # String | 
}

begin
  #修改三方应用图标
  result = api_instance.app_update_oauth_app_logo(identapp, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_update_oauth_app_logo: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **String**| 三方应用标识 | 
 **app** | **String**| 应用标识 | 
 **logo** | **String**|  | [optional] 

### Return type

[**OauthAppBean**](OauthAppBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json



# **app_update_status**
> BOOLEAN app_update_status(app, vcode, status)

修改应用的状态

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::AppsApi.new
app = 'app_example' # String | 应用标识
vcode = 'vcode_example' # String | 验证码
status = 56 # Integer | 新状态


begin
  #修改应用的状态
  result = api_instance.app_update_status(app, vcode, status)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling AppsApi->app_update_status: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **vcode** | **String**| 验证码 | 
 **status** | **Integer**| 新状态 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



