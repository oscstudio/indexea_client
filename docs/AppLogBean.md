# IndexeaClient::AppLogBean

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**app** | **Integer** |  | [optional] 
**account** | [**AppLogAccount**](AppLogAccount.md) |  | [optional] 
**index** | **Integer** |  | [optional] 
**widget** | **Integer** |  | [optional] 
**query** | **Integer** |  | [optional] 
**recomm** | **Integer** |  | [optional] 
**type** | **Integer** | 日志类型值 | [optional] 
**type_desc** | **String** | 日志类型说明 | [optional] 
**log** | **String** | 日志内容 | [optional] 
**detail** | **Object** |  | [optional] 
**ip** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 

