# IndexeaClient::RecommendApi

All URIs are relative to *https://api.indexea.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**recommend_click**](RecommendApi.md#recommend_click) | **POST** /recommend/{ident}/click | 推荐结果点击行为收集
[**recommend_create**](RecommendApi.md#recommend_create) | **POST** /recommends/{app} | 创建新的推荐
[**recommend_delete**](RecommendApi.md#recommend_delete) | **DELETE** /recommends/{app} | 删除推荐
[**recommend_detail**](RecommendApi.md#recommend_detail) | **GET** /recommend/{ident} | 获取推荐的记录列表
[**recommend_fetch**](RecommendApi.md#recommend_fetch) | **POST** /recommend/{ident} | 获取推荐的记录列表
[**recommend_list**](RecommendApi.md#recommend_list) | **GET** /recommends/{app} | 获取已定义的推荐列表
[**recommend_update**](RecommendApi.md#recommend_update) | **PUT** /recommends/{app} | 更新推荐信息

# **recommend_click**
> BOOLEAN recommend_click(ident, action_id, doc_id, opts)

推荐结果点击行为收集

该接口主要用于记录用户对推荐结果的点击行为

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::RecommendApi.new
ident = 'ident_example' # String | 推荐的唯一标识
action_id = 'action_id_example' # String | 对应推荐行为编号
doc_id = 'doc_id_example' # String | 对应索引中的内部记录编号
opts = { 
  userid: 'userid_example', # String | 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64
  x_token: 'x_token_example' # String | 如果要使用非发布的组件，需要组件作者授权
}

begin
  #推荐结果点击行为收集
  result = api_instance.recommend_click(ident, action_id, doc_id, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecommendApi->recommend_click: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **String**| 推荐的唯一标识 | 
 **action_id** | **String**| 对应推荐行为编号 | 
 **doc_id** | **String**| 对应索引中的内部记录编号 | 
 **userid** | **String**| 搜索者的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 

### Return type

**BOOLEAN**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **recommend_create**
> RecommendBean recommend_create(bodyapp)

创建新的推荐

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecommendApi.new
body = IndexeaClient::RecommendBean.new # RecommendBean | 推荐信息
app = 'app_example' # String | 应用标识


begin
  #创建新的推荐
  result = api_instance.recommend_create(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecommendApi->recommend_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RecommendBean**](RecommendBean.md)| 推荐信息 | 
 **app** | **String**| 应用标识 | 

### Return type

[**RecommendBean**](RecommendBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **recommend_delete**
> BOOLEAN recommend_delete(app, id)

删除推荐

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecommendApi.new
app = 'app_example' # String | 应用标识
id = 56 # Integer | 推荐编号


begin
  #删除推荐
  result = api_instance.recommend_delete(app, id)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecommendApi->recommend_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 
 **id** | **Integer**| 推荐编号 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **recommend_detail**
> RecommendBean recommend_detail(ident, opts)

获取推荐的记录列表

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::RecommendApi.new
ident = 'ident_example' # String | 推荐定义的标识
opts = { 
  x_token: 'x_token_example' # String | 如果要使用非发布的组件，需要组件作者授权
}

begin
  #获取推荐的记录列表
  result = api_instance.recommend_detail(ident, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecommendApi->recommend_detail: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **String**| 推荐定义的标识 | 
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 

### Return type

[**RecommendBean**](RecommendBean.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **recommend_fetch**
> Object recommend_fetch(ident, opts)

获取推荐的记录列表

### Example
```ruby
# load the gem
require 'indexea_client'

api_instance = IndexeaClient::RecommendApi.new
ident = 'ident_example' # String | 推荐定义的标识
opts = { 
  x_token: 'x_token_example', # String | 如果要使用非发布的组件，需要组件作者授权
  userid: 'userid_example', # String | 访客的唯一标识，该标识由搜索前端生成，长度不超过64
  condition: {'key' => 'condition_example'}, # Hash<String, String> | 获取某个记录的参数,例如 id=11223(后端将使用 term query 进行匹配)
  from: 0, # Integer | 起始值
  count: 10 # Integer | 推荐的记录数
}

begin
  #获取推荐的记录列表
  result = api_instance.recommend_fetch(ident, opts)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecommendApi->recommend_fetch: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **String**| 推荐定义的标识 | 
 **x_token** | **String**| 如果要使用非发布的组件，需要组件作者授权 | [optional] 
 **userid** | **String**| 访客的唯一标识，该标识由搜索前端生成，长度不超过64 | [optional] 
 **condition** | [**Hash&lt;String, String&gt;**](String.md)| 获取某个记录的参数,例如 id&#x3D;11223(后端将使用 term query 进行匹配) | [optional] 
 **from** | **Integer**| 起始值 | [optional] [default to 0]
 **count** | **Integer**| 推荐的记录数 | [optional] [default to 10]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **recommend_list**
> Array&lt;RecommendBean&gt; recommend_list(app)

获取已定义的推荐列表

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecommendApi.new
app = 'app_example' # String | 应用标识


begin
  #获取已定义的推荐列表
  result = api_instance.recommend_list(app)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecommendApi->recommend_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **app** | **String**| 应用标识 | 

### Return type

[**Array&lt;RecommendBean&gt;**](RecommendBean.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **recommend_update**
> BOOLEAN recommend_update(bodyapp)

更新推荐信息

### Example
```ruby
# load the gem
require 'indexea_client'
# setup authorization
IndexeaClient.configure do |config|
end

api_instance = IndexeaClient::RecommendApi.new
body = IndexeaClient::RecommendBean.new # RecommendBean | 推荐信息
app = 'app_example' # String | 应用标识


begin
  #更新推荐信息
  result = api_instance.recommend_update(bodyapp)
  p result
rescue IndexeaClient::ApiError => e
  puts "Exception when calling RecommendApi->recommend_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RecommendBean**](RecommendBean.md)| 推荐信息 | 
 **app** | **String**| 应用标识 | 

### Return type

**BOOLEAN**

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



